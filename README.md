# Automata libraries

Finite State Automata using the [Stateless] Project.

* [Source Code](#Source-Code)
* [License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Repository Owner](#Repository-Owner)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Automata] - Automata Libraries

```
git clone git@bitbucket.org:davidhary/dn.automata.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\algorithms\automata
```

where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]


<a name="Acknowledgments"></a>
## Acknowledgments
* [Stateless]
* [Moore State Machine]
* [Its all a remix] -- we are but a spec on the shoulders of giants
* [John Simmons] - outlaw programmer
* [Stack overflow]

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
* [Automata]  
* [Moore State Machine]  
* [Stateless]

<a name="Closed-software"></a>
### Closed software
None

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Automata]: https://bitbucket.org/davidhary/dn.Automata
[Moore State Machine]: http://www.codeproject.com/KB/recipes/MooreMachine.aspx
[Stateless]: https://github.com/dotnet-state-machine/stateless.git

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm


