using System;
using Newtonsoft.Json;
using Stateless;

namespace JsonExample
{
    public class Member
    {
        private enum MemberTriggers
        {
            Suspend,
            Terminate,
            Reactivate
        }
        public enum MembershipState
        {
            Inactive,
            Active,
            Terminated
        }
        public MembershipState State => this._StateMachine.State;
        public string Name { get; }
        private readonly StateMachine<MembershipState, MemberTriggers> _StateMachine;

        public Member(string name)
        {
            this._StateMachine = new StateMachine<MembershipState, MemberTriggers>(MembershipState.Active);
            this.Name = name;

            this.ConfigureStateMachine();
        }

        [JsonConstructor]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private Member(string state, string name)
        {
            var memberState = (MembershipState) Enum.Parse(typeof(MembershipState), state);
            this._StateMachine = new StateMachine<MembershipState, MemberTriggers>(memberState);
            this.Name = name;

            this.ConfigureStateMachine();
        }

        private void ConfigureStateMachine()
        {
            _ = this._StateMachine.Configure( MembershipState.Active )
                .Permit( MemberTriggers.Suspend, MembershipState.Inactive )
                .Permit( MemberTriggers.Terminate, MembershipState.Terminated );

            _ = this._StateMachine.Configure( MembershipState.Inactive )
                .Permit( MemberTriggers.Reactivate, MembershipState.Active )
                .Permit( MemberTriggers.Terminate, MembershipState.Terminated );

            _ = this._StateMachine.Configure( MembershipState.Terminated )
                .Permit( MemberTriggers.Reactivate, MembershipState.Active );
        }

        public void Terminate()
        {
            this._StateMachine.Fire(MemberTriggers.Terminate);
        }

        public void Suspend()
        {
            this._StateMachine.Fire(MemberTriggers.Suspend);
        }

        public void Reactivate()
        {
            this._StateMachine.Fire(MemberTriggers.Reactivate);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Member FromJson(string jsonString)
        {
            return JsonConvert.DeserializeObject<Member>(jsonString);
        }

        public bool Equals(Member anotherMember)
        {
            return ((this.State == anotherMember.State) && (this.Name == anotherMember.Name));
        }
    }


    

}
