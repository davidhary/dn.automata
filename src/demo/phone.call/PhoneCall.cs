using System;

using Stateless;
using Stateless.Graph;

namespace PhoneCallExample
{
    public class PhoneCall
    {
        private enum Trigger
        {
            CallDialed,
            CallConnected,
            LeftMessage,
            PlacedOnHold,
            TakenOffHold,
            PhoneHurledAgainstWall,
            MuteMicrophone,
            UnmuteMicrophone,
            SetVolume
        }

        private enum State
        {
            OffHook,
            Ringing,
            Connected,
            OnHold,
            PhoneDestroyed
        }

        private State _State = State.OffHook;
        private readonly StateMachine<State, Trigger> _Machine;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<int> _SetVolumeTrigger;
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<string> _SetCalleeTrigger;
        private readonly string _Caller;
        private string _Callee;

        public PhoneCall( string caller )
        {
            this._Caller = caller;
            this._Machine = new StateMachine<State, Trigger>( () => this._State, s => this._State = s );

            this._SetVolumeTrigger = this._Machine.SetTriggerParameters<int>( Trigger.SetVolume );
            this._SetCalleeTrigger = this._Machine.SetTriggerParameters<string>( Trigger.CallDialed );

            _ = this._Machine.Configure( State.OffHook )
                .Permit( Trigger.CallDialed, State.Ringing );

            _ = this._Machine.Configure( State.Ringing )
                .OnEntryFrom( this._SetCalleeTrigger, callee => this.OnDialed( callee ), "Caller number to call" )
                .Permit( Trigger.CallConnected, State.Connected );

            _ = this._Machine.Configure( State.Connected )
                .OnEntry( t => StartCallTimer() )
                .OnExit( t => StopCallTimer() )
                .InternalTransition( Trigger.MuteMicrophone, t => OnMute() )
                .InternalTransition( Trigger.UnmuteMicrophone, t => OnUnmute() )
                .InternalTransition<int>( this._SetVolumeTrigger, ( volume, t ) => OnSetVolume( volume ) )
                .Permit( Trigger.LeftMessage, State.OffHook )
                .Permit( Trigger.PlacedOnHold, State.OnHold );

            _ = this._Machine.Configure( State.OnHold )
                .SubstateOf( State.Connected )
                .Permit( Trigger.TakenOffHold, State.Connected )
                .Permit( Trigger.PhoneHurledAgainstWall, State.PhoneDestroyed );
        }

        private static void OnSetVolume( int volume )
        {
            Console.WriteLine( "Volume set to " + volume + "!" );
        }

        private static void OnUnmute()
        {
            Console.WriteLine( "Microphone unmuted!" );
        }

        private static void OnMute()
        {
            Console.WriteLine( "Microphone muted!" );
        }

        private void OnDialed( string callee )
        {
            this._Callee = callee;
            Console.WriteLine( "[Phone Call] placed for : [{0}]", this._Callee );
        }

        private static void StartCallTimer()
        {
            Console.WriteLine( "[Timer:] Call started at {0}", DateTimeOffset.Now );
        }

        private static void StopCallTimer()
        {
            Console.WriteLine( "[Timer:] Call ended at {0}", DateTimeOffset.Now );
        }

        public void Mute()
        {
            this._Machine.Fire( Trigger.MuteMicrophone );
        }

        public void Unmute()
        {
            this._Machine.Fire( Trigger.UnmuteMicrophone );
        }

        public void SetVolume( int volume )
        {
            this._Machine.Fire( this._SetVolumeTrigger, volume );
        }

        public void Print()
        {
            Console.WriteLine( "[{1}] placed call and [Status:] {0}", this._Machine.State, this._Caller );
        }

        public void Dialed( string callee )
        {
            this._Machine.Fire( this._SetCalleeTrigger, callee );
        }

        public void Connected()
        {
            this._Machine.Fire( Trigger.CallConnected );
        }

        public void Hold()
        {
            this._Machine.Fire( Trigger.PlacedOnHold );
        }

        public void Resume()
        {
            this._Machine.Fire( Trigger.TakenOffHold );
        }

        public string ToDotGraph()
        {
            return UmlDotGraph.Format( this._Machine.GetInfo() );
        }
    }
}
