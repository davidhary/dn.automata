using System;

namespace PhoneCallExample
{
    internal partial class Program
    {
        private static void Main()
        {
            Program.RunPhoneCall();
            Console.WriteLine( "Done; Press any key..." );
            _ = Console.ReadKey( true );

        }
    }
}
