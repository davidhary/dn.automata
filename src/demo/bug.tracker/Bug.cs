using System;
using Stateless;
using Stateless.Graph;

namespace BugTrackerExample
{
    public class Bug
    {
        private enum State { Open, Assigned, Deferred, Closed }

        private enum Trigger { Assign, Defer, Close }

        private readonly StateMachine<State, Trigger> _Machine;
        // The TriggerWithParameters object is used when a trigger requires a payload.
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<string> _AssignTrigger;

        private readonly string _Title;
        private string _Assignee;


        /// <summary>
        /// Constructor for the Bug class
        /// </summary>
        /// <param name="title">The title of the bug report</param>
        public Bug(string title)
        {
            this._Title = title;

            // Instantiate a new state machine in the Open state
            this._Machine = new StateMachine<State, Trigger>(State.Open);

            // Instantiate a new trigger with a parameter. 
            this._AssignTrigger = this._Machine.SetTriggerParameters<string>(Trigger.Assign);

            // Configure the Open state
            _ = this._Machine.Configure( State.Open )
                .Permit( Trigger.Assign, State.Assigned );

            // Configure the Assigned state
            _ = this._Machine.Configure( State.Assigned )
                .SubstateOf( State.Open )
                .OnEntryFrom( this._AssignTrigger, this.OnAssigned )  // This is where the TriggerWithParameters is used. Note that the TriggerWithParameters object is used, not something from the enum
                .PermitReentry( Trigger.Assign )
                .Permit( Trigger.Close, State.Closed )
                .Permit( Trigger.Defer, State.Deferred )
                .OnExit( this.OnDeassigned );

            // Configure the Deferred state
            _ = this._Machine.Configure( State.Deferred )
                .OnEntry( () => this._Assignee = null )
                .Permit( Trigger.Assign, State.Assigned );
        }

        public void Close()
        {
            this._Machine.Fire(Trigger.Close);
        }

        public void Assign(string assignee)
        {
            // This is how a trigger with parameter is used, the parameter is supplied to the state machine as a parameter to the Fire method.
            this._Machine.Fire( this._AssignTrigger, assignee);
        }

        public bool CanAssign => this._Machine.CanFire(Trigger.Assign);

        public void Defer()
        {
            this._Machine.Fire(Trigger.Defer);
        }
        /// <summary>
        /// This method is called automatically when the Assigned state is entered, but only when the trigger is _assignTrigger.
        /// </summary>
        /// <param name="assignee"></param>
        private void OnAssigned(string assignee)
        {
            if ( this._Assignee != null && assignee != this._Assignee )
                this.SendEmailToAssignee("Don't forget to help the new employee!");

            this._Assignee = assignee;
            this.SendEmailToAssignee("You own it.");
        }
        /// <summary>
        /// This method is called when the state machine exits the Assigned state
        /// </summary>
        private void OnDeassigned()
        {
            this.SendEmailToAssignee("You're off the hook.");
        }

        private void SendEmailToAssignee(string message)
        {
            Console.WriteLine("{0}, RE {1}: {2}", this._Assignee, this._Title, message);
        }

        public string ToDotGraph()
        {
            return UmlDotGraph.Format( this._Machine.GetInfo());
        }
    }
}
