using System;

namespace isr.Automata.Finite.MSTest
{
    /// <summary>   A helper methods. </summary>
    /// <remarks>   David, 2021-03-22. </remarks>
    internal static class HelperMethods
    {

        /// <summary>   Wait until. </summary>
        /// <remarks>   David, 2021-03-22. </remarks>
        /// <param name="timeout">      The timeout. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool WaitUntil( this TimeSpan timeout, Func<bool> predicate )
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            bool done;
            do
            {
                done = predicate();
            } while ( sw.Elapsed < timeout && !done );
            return done;
        }

    }
}
