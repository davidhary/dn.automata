using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using isr.Automata.Finite.Engines;
using Stateless;
using isr.Automata.Finite.MSTest;

namespace isr.Automata.FiniteTests
{
    /// <summary> This is a test class for Seebeck state machine automata. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para>
    /// </remarks>
    [TestClass()]
    public class SeebeckEngineTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary> Gets or sets the Seebeck engine. </summary>
        /// <value> The Seebeck engine. </value>
        public SeebeckEngine SeebeckEngine { get; private set; }

        /// <summary> Assign Seebeck automaton. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignSeebeckAutomaton( SeebeckEngine value )
        {
            if ( this.SeebeckEngine is object )
            {
                this.SeebeckEngine = null;
            }

            if ( value is object )
            {
                this.SeebeckEngine = value;
                if ( this.SeebeckEngine.UsingFireAsync )
                {
                    this.SeebeckEngine.StateMachine.OnTransitionedAsync( ( t ) => Task.Run( () => this.OnTransitioned( t ) ) );
                }
                else
                {
                    this.SeebeckEngine.StateMachine.OnTransitioned( t => this.OnTransitioned( t ) );
                }
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnTransitioned( StateMachine<SeebeckState, SeebeckTrigger>.Transition transition )
        {
            string activity;
            if ( transition is null )
            {
                activity = $"Entering the {this.SeebeckEngine.StateMachine.State} state";
                System.Diagnostics.Trace.TraceInformation( activity );
                this.OnActivated( this.SeebeckEngine.StateMachine.State );
            }
            else
            {
                activity = $"transitioning {this.SeebeckEngine.Name}: {transition.Source} --> {transition.Destination}";
                System.Diagnostics.Trace.TraceInformation( activity );
                if ( transition.IsReentry )
                {
                    this.OnReentry( transition.Destination );
                }
                else
                {
                    this.OnExit( transition.Source );
                    this.OnActivated( transition.Destination );
                }
            }
        }

        /// <summary> Executes the 'exit' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="source"> Source for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void OnExit( SeebeckState source )
        {
            switch ( source )
            {
                case SeebeckState.Soaking:
                    {
                        break;
                    }
                    // stop monitoring temperatures
            }
        }

        /// <summary> Executes the 'reentry' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void OnReentry( SeebeckState destination )
        {
            switch ( destination )
            {
                case SeebeckState.Soaking:
                    {
                        break;
                    }
                    // read temperatures
            }
        }

        /// <summary> Executes the 'activated' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void OnActivated( SeebeckState destination )
        {
            switch ( destination )
            {
                case SeebeckState.Soaking:
                    {
                        break;
                    }
                    // start monitoring temperatures
            }
        }

        /// <summary> (Unit Test Method) tests Seebeck engine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void SeebeckEngineTest()
        {
            System.Diagnostics.Trace.TraceInformation( "Begin Seebeck engine test" );
            using var automata = new SeebeckEngine( "Seebeck Sequencer" );
            this.AssignSeebeckAutomaton( automata );
            _ = this.SeebeckEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected initial state" );
            System.Diagnostics.Trace.TraceInformation( "Waiting for substrate present; engine should start monitoring part present" );
            this.SeebeckEngine.AwaitPartPresent();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Inserting, "this is the expected awaiting for part present state" );
            System.Diagnostics.Trace.TraceInformation( "Inserting a substrate; engine should start monitoring temperature" );
            this.SeebeckEngine.SoakPart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Soaking, "this is the expected part present state" );
            System.Diagnostics.Trace.TraceInformation( "Part at temp, engine should start contacting" );
            this.SeebeckEngine.ConnectPart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Contacting, "this is the expected at temp state" );
            System.Diagnostics.Trace.TraceInformation( "Part contacted, engine should start measuring" );
            this.SeebeckEngine.MeasurePart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Measuring, "this is the expected state after part is contacted" );
            System.Diagnostics.Trace.TraceInformation( "Part measured, engine should removing the part" );
            this.SeebeckEngine.RemovePart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Removing, "this is the expected state after part is measured" );
            System.Diagnostics.Trace.TraceInformation( "Part removed, engine should show party not present" );
            this.SeebeckEngine.Initial();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected state after part is removed" );
        }

        /// <summary> (Unit Test Method) tests Seebeck engine asynchronous. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void SeebeckEngineAsyncTest()
        {
            System.Diagnostics.Trace.TraceInformation( "Begin Seebeck engine test" );
            using var automata = new SeebeckEngine( "Seebeck Sequencer" ) {
                UsingFireAsync = true
            };
            this.AssignSeebeckAutomaton( automata );
            _ = this.SeebeckEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected initial state" );

            System.Diagnostics.Trace.TraceInformation( "Waiting for substrate present; engine should start monitoring part present" );
            this.SeebeckEngine.AwaitPartPresent();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.SeebeckEngine.StateMachine.State == SeebeckState.Inserting );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Inserting, "this is the expected awaiting for part present state" );

            System.Diagnostics.Trace.TraceInformation( "Soaking a substrate; engine should start monitoring temperature" );
            this.SeebeckEngine.SoakPart();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.SeebeckEngine.StateMachine.State == SeebeckState.Soaking );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Soaking, "this is the expected part present state" );

            System.Diagnostics.Trace.TraceInformation( "Part at temp, engine should start contacting" );
            this.SeebeckEngine.ConnectPart();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.SeebeckEngine.StateMachine.State == SeebeckState.Contacting );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Contacting, "this is the expected at temp state" );

            System.Diagnostics.Trace.TraceInformation( "Part contacted, engine should start measuring" );
            this.SeebeckEngine.MeasurePart();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.SeebeckEngine.StateMachine.State == SeebeckState.Measuring );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Measuring, "this is the expected state after part is contacted" );

            System.Diagnostics.Trace.TraceInformation( "Part measured, engine should removing the part" );
            this.SeebeckEngine.RemovePart();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.SeebeckEngine.StateMachine.State == SeebeckState.Removing );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Removing, "this is the expected state after part is measured" );

            System.Diagnostics.Trace.TraceInformation( "Part removed, engine should show party not present" );
            this.SeebeckEngine.Initial();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.SeebeckEngine.StateMachine.State == SeebeckState.Initial );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected state after part is removed" );
        }
    }
}
