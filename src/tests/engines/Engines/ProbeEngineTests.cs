using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using isr.Automata.Finite.Engines;

using Stateless;
using isr.Automata.Finite.MSTest;

namespace isr.Automata.FiniteTests
{

    /// <summary> This is a test class for Probe state machine automata. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para>
    /// </remarks>
    [TestClass()]
    public partial class ProbeEngineTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        #endregion


        /// <summary> Gets or sets the probe engine. </summary>
        /// <value> The probe engine. </value>
        public ProbeEngine ProbeEngine { get; private set; }

        /// <summary> Assign Probe automaton. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignProbeAutomaton( ProbeEngine value )
        {
            if ( this.ProbeEngine is object )
            {
                this.ProbeEngine = null;
            }

            if ( value is object )
            {
                this.ProbeEngine = value;
                if ( this.ProbeEngine.UsingFireAsync )
                {
                    this.ProbeEngine.StateMachine.OnTransitionedAsync( ( t ) => Task.Run( () => this.OnTransitioned( t ) ) );
                }
                else
                {
                    this.ProbeEngine.StateMachine.OnTransitioned( t => this.OnTransitioned( t ) );
                }
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnTransitioned( StateMachine<ProbeState, ProbeTrigger>.Transition transition )
        {
            string activity;
            if ( transition is null )
            {
                activity = $"Entering the {this.ProbeEngine.StateMachine.State} state";
                System.Diagnostics.Trace.TraceInformation( activity );
                this.OnActivated( this.ProbeEngine.StateMachine.State );
            }
            else
            {
                activity = $"transitioning {this.ProbeEngine.Name}: {transition.Source} --> {transition.Destination}";
                System.Diagnostics.Trace.TraceInformation( activity );
                if ( transition.IsReentry )
                {
                    this.OnReentry( transition.Destination );
                }
                else
                {
                    this.OnExit( transition.Source );
                    this.OnActivated( transition.Destination );
                }
            }
        }

        /// <summary> Executes the 'exit' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="source"> Source for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void OnExit( ProbeState source )
        {
            switch ( source )
            {
                case ProbeState.Lifting:
                    {
                        break;
                    }
            }
        }

        /// <summary> Executes the 'reentry' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void OnReentry( ProbeState destination )
        {
            switch ( destination )
            {
                case ProbeState.Lifting:
                    {
                        break;
                    }
                    // read temperatures
            }
        }

        /// <summary> Executes the 'activated' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void OnActivated( ProbeState destination )
        {
            switch ( destination )
            {
                case ProbeState.Lifting:
                    {
                        break;
                    }
                    // start monitoring temperatures
            }
        }

        /// <summary> (Unit Test Method) tests probe engine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void ProbeEngineTest()
        {
            System.Diagnostics.Trace.TraceInformation( "Begin Probe engine test" );
            using var Automata = new ProbeEngine( "Probe Sequencer" );
            this.AssignProbeAutomaton( Automata );
            _ = this.ProbeEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Initial, "this is the expected initial state" );
            System.Diagnostics.Trace.TraceInformation( "Lowering the probe; engine should start monitoring resistance" );
            this.ProbeEngine.Lower();
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Lowering, "this is the lowering state" );
            System.Diagnostics.Trace.TraceInformation( "Contact detected, engine should start probing" );
            this.ProbeEngine.Probe();
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Probing, "this is the expected at temp state" );
            System.Diagnostics.Trace.TraceInformation( "Part measured, engine should start lifting" );
            this.ProbeEngine.Lift();
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Lifting, "this is the expected state after part is measured" );
            System.Diagnostics.Trace.TraceInformation( "Probe lifted, engine should go to initial state" );
            this.ProbeEngine.Initial();
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Initial, "this is the expected state after lifting is done" );
        }

        /// <summary> (Unit Test Method) tests probe engine asynchronous. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void ProbeEngineAsyncTest()
        {
            System.Diagnostics.Trace.TraceInformation( "Begin Probe engine test" );
            using var automata = new ProbeEngine( "Probe Sequencer" ) {
                UsingFireAsync = true
            };
            this.AssignProbeAutomaton( automata );
            _ = this.ProbeEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Initial, "this is the expected initial state" );

            System.Diagnostics.Trace.TraceInformation( "Lowering the probe; engine should start monitoring resistance" );
            this.ProbeEngine.Lower();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.ProbeEngine.StateMachine.State == ProbeState.Lowering );
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Lowering, "this is the lowering state" );

            System.Diagnostics.Trace.TraceInformation( "Contact detected, engine should start probing" );
            this.ProbeEngine.Probe();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.ProbeEngine.StateMachine.State == ProbeState.Probing );
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Probing, "this is the expected at temp state" );

            System.Diagnostics.Trace.TraceInformation( "Part measured, engine should start lifting" );
            this.ProbeEngine.Lift();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.ProbeEngine.StateMachine.State == ProbeState.Lifting );
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Lifting, "this is the expected state after part is measured" );

            System.Diagnostics.Trace.TraceInformation( "Probe lifted, engine should go to initial state" );
            this.ProbeEngine.Initial();
            _ = TimeSpan.FromMilliseconds( 100 ).WaitUntil( () => this.ProbeEngine.StateMachine.State == ProbeState.Initial );
            _ = this.ProbeEngine.StateMachine.State.Should().Be( ProbeState.Initial, "this is the expected state after lifting is done" );
        }

    }
}
