using System;

namespace isr.Automata.Finite
{

    /// <summary> Provides information on a state machine. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-08-26 </para>
    /// </remarks>
    public class StateMachineInfo
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        public StateMachineInfo() : base()
        {
            this.CurrentState = string.Empty;
            this.LastTransactionDestination = string.Empty;
            this.LastTransactionSource = string.Empty;
            this.LastTransactionTrigger = string.Empty;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="currentState"> The current state. </param>
        public StateMachineInfo( string currentState ) : this()
        {
            this.CurrentState = currentState;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="currentState">               The current state. </param>
        /// <param name="lastTransactionSource">      The last transaction source. </param>
        /// <param name="lastTransactionDestination"> The last transaction destination. </param>
        /// <param name="lastTransactionTrigger">     The last transaction trigger. </param>
        public StateMachineInfo( string currentState, string lastTransactionSource, string lastTransactionDestination, string lastTransactionTrigger ) : this()
        {
            this.CurrentState = currentState;
            this.LastTransactionSource = lastTransactionSource;
            this.LastTransactionDestination = lastTransactionDestination;
            this.LastTransactionTrigger = lastTransactionTrigger;
        }

        /// <summary> The success. </summary>

        private static StateMachineInfo _Success;

        /// <summary> Gets the success. </summary>
        /// <value> The success. </value>
        public static StateMachineInfo Success
        {
            get {
                if ( _Success is null )
                {
                    _Success = new StateMachineInfo();
                }

                return _Success;
            }
        }

        /// <summary> Gets or sets the current state. </summary>
        /// <value> The current state. </value>
        public string CurrentState { get; private set; }

        /// <summary> Gets or sets the last transaction source. </summary>
        /// <value> The last transaction source. </value>
        public string LastTransactionSource { get; private set; }

        /// <summary> Gets or sets the last transaction destination. </summary>
        /// <value> The last transaction destination. </value>
        public string LastTransactionDestination { get; private set; }

        /// <summary> Gets or sets the last transaction trigger. </summary>
        /// <value> The last transaction trigger. </value>
        public string LastTransactionTrigger { get; private set; }

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="exception"> The exception receiving the added data. </param>
        public void AddExceptionData( Exception exception )
        {
            if ( !(exception is null || string.IsNullOrEmpty( this.CurrentState )) )
            {
                int count = exception.Data.Count;
                exception.Data.Add( $"{count}-{nameof( isr.Automata.Finite.StateMachineInfo.CurrentState )}", this.CurrentState );
                if ( !string.IsNullOrEmpty( this.LastTransactionSource ) )
                {
                    exception.Data.Add( $"{count}-{nameof( isr.Automata.Finite.StateMachineInfo.LastTransactionSource )}", this.LastTransactionSource );
                    exception.Data.Add( $"{count}-{nameof( isr.Automata.Finite.StateMachineInfo.LastTransactionDestination )}", this.LastTransactionDestination );
                }
            }
        }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public new string ToString()
        {
            if ( string.IsNullOrEmpty( this.CurrentState ) )
            {
                return base.ToString();
            }
            else
            {
                var stringBuilder = new System.Text.StringBuilder();
                var builder = new System.Text.StringBuilder( base.ToString() );
                _ = builder.AppendLine( "State machine engine Info:" );
                _ = builder.AppendLine( $"{ nameof( isr.Automata.Finite.StateMachineInfo.CurrentState )}: {this.CurrentState}" );
                if ( !string.IsNullOrEmpty( this.LastTransactionSource ) )
                {
                    _ = builder.AppendLine( $"{ nameof( isr.Automata.Finite.StateMachineInfo.LastTransactionSource )}: {this.LastTransactionSource}" );
                    _ = builder.AppendLine( $"{ nameof( isr.Automata.Finite.StateMachineInfo.LastTransactionDestination )}: {this.LastTransactionDestination}" );
                    _ = builder.Append( $"{ nameof( isr.Automata.Finite.StateMachineInfo.LastTransactionTrigger )}: {this.LastTransactionTrigger}" );
                }

                return stringBuilder.ToString();
            }
        }
    }
}
