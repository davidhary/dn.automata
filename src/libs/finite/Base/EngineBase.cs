using System;
using System.ComponentModel;
using System.Diagnostics;

using Stateless;

namespace isr.Automata.Finite
{

    /// <summary> A State Machine Engine base class. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public abstract class EngineBase<TState, TTrigger> : INotifyPropertyChanged, IDisposable
    {

        #region " CONSTRUCTOR "

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="stateMachine"> The state machine. </param>
        protected EngineBase( string name, StateMachine<TState, TTrigger> stateMachine ) : base()
        {
            this.Name = name;
            this.StateMachine = stateMachine;
            this.StateMachine.OnTransitioned( t => this.OnTransitioned( t ) );
            this.EngineFailedNotifier = new ActionNotifier<System.IO.ErrorEventArgs>();
            this._SlimLock = new System.Threading.ReaderWriterLockSlim();
            this.EngagedStates = new System.Collections.ObjectModel.Collection<TState>();
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources optionally releases
        /// managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this._SlimLock?.Dispose();
                    this._SlimLock = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        ~EngineBase()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " EVENT HANDLING "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " STATE MACHINE "

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; private set; }

        /// <summary> Gets or sets the state machine. </summary>
        /// <value> The state machine. </value>
        public StateMachine<TState, TTrigger> StateMachine { get; private set; }

        /// <summary> The slim lock. </summary>
        private System.Threading.ReaderWriterLockSlim _SlimLock;

        /// <summary> True if stop requested. </summary>
        private bool _StopRequested;

        /// <summary> Gets or sets the StopRequested. </summary>
        /// <value> The stop requested. </value>
        public bool StopRequested
        {
            get {
                this._SlimLock.EnterReadLock();
                try
                {
                    return this._StopRequested;
                }
                finally
                {
                    this._SlimLock.ExitReadLock();
                }
            }

            set {
                this._SlimLock.EnterWriteLock();
                try
                {
                    this._StopRequested = value;
                }
                finally
                {
                    this._SlimLock.ExitWriteLock();
                }
            }
        }

        /// <summary> The stopping stop watch. </summary>
        private readonly Stopwatch _StoppingStopWatch = new();

        /// <summary> The stopping timeout. </summary>
        private TimeSpan _StoppingTimeout;

        /// <summary> Begins a stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="timeout"> The timeout. </param>
        public void BeginStopRequest( TimeSpan timeout )
        {
            this.StopRequested = true;
            this._StoppingStopWatch.Restart();
            this._StoppingTimeout = timeout;
            // get going 
            this.ProcessStopRequest();
        }

        /// <summary> Process the stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public virtual void ProcessStopRequest()
        {
        }

        /// <summary> Determines if we stopping timeout elapsed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool StoppingTimeoutElapsed()
        {
            return this._StoppingStopWatch.Elapsed > this._StoppingTimeout;
        }

        /// <summary> Ends stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void EndStopRequest()
        {
            this.StopRequested = false;
            this._StoppingStopWatch.Stop();
        }

        #endregion

        #region " SPECIAL STATES "

        /// <summary> Gets the engaged states. </summary>
        /// <remarks>
        /// Engaged states are used to tag states which require the state machine special action for
        /// disengagement. For example, if the state machine monitors part presence, all states where the
        /// part is present could be tagged as 'Engaged' indicating that the part must be removed before
        /// the state machine can be declared as disengaged.
        /// </remarks>
        /// <value> The active states. </value>
        public System.Collections.ObjectModel.Collection<TState> EngagedStates { get; private set; }

        /// <summary>
        /// Gets the sentinel indicating of the engine is engaged in one of the engaged states.
        /// </summary>
        /// <value> The engaged. </value>
        public bool Engaged => this.EngagedStates.Contains( this.StateMachine.State );

        #endregion

        #region " STATE MACHINE INFO "

        /// <summary> Gets the current state. </summary>
        /// <value> The current state. </value>
        public TState CurrentState => this.StateMachine.State;

        /// <summary> Report state change. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        protected virtual void OnTransitioned( StateMachine<TState, TTrigger>.Transition transition )
        {
            this.LastTransition = transition;
            this.NotifyPropertyChanged( nameof( this.CurrentState ) );
        }

        /// <summary> The last transition. </summary>
        private StateMachine<TState, TTrigger>.Transition _LastTransition;

        /// <summary> Gets or sets the last transition. </summary>
        /// <value> The last transition. </value>
        public StateMachine<TState, TTrigger>.Transition LastTransition
        {
            get => this._LastTransition;

            set {
                this._LastTransition = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Gets information describing the state machine. </summary>
        /// <value> Information describing the state machine. </value>
        public StateMachineInfo StateMachineInfo => this.LastTransition is null ? new StateMachineInfo( this.CurrentState.ToString() ) : new StateMachineInfo( this.CurrentState.ToString(), this.LastTransition.Source.ToString(), this.LastTransition.Destination.ToString(), this.LastTransition.Trigger.ToString() );


        #endregion

        #region " ENGINE FAILED NOTIFICATIONS "

        /// <summary> Asynchronously reports engine failure to all registered delegates. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="e"> Specifies the <see cref="System.EventArgs">system event arguments</see> </param>
        public virtual async void OnEngineFailedAsync( System.IO.ErrorEventArgs e )
        {
            await this.EngineFailedNotifier.InvokeAsync( e );
        }

        /// <summary> Synchronously reports engine failure to all registered delegates. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="e"> Specifies the <see cref="System.EventArgs">system event arguments</see> </param>
        public virtual void OnEngineFailed( System.IO.ErrorEventArgs e )
        {
            this.EngineFailedNotifier.Invoke( e );
        }

        /// <summary> Reports engine failure to all registered delegates. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="exception"> The exception. </param>
        public virtual void OnEngineFailed( Exception exception )
        {
            this.OnEngineFailed( exception, "State machine {0} failed.", this.Name );
        }

        /// <summary> Reports engine failure to all registered delegates. </summary>
        /// <remarks> David, 2021-04-22. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="exception"> The exception. </param>
        public virtual void OnEngineFailed( string activity, Exception exception )
        {
            this.OnEngineFailed( exception, $"State machine {this.Name } failed {activity}." );
        }

        /// <summary> Reports engine failure to all registered delegates. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="exception">     The exception. </param>
        /// <param name="detailsFormat"> The details format. </param>
        /// <param name="args">          A variable-length parameters list containing arguments. </param>
        public virtual void OnEngineFailed( Exception exception, string detailsFormat, params object[] args )
        {
            this.OnEngineFailed( new System.IO.ErrorEventArgs( new EngineException( this.StateMachineInfo, exception, detailsFormat, args ) ) );
        }

        /// <summary> Raises the Failed event. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="detailsFormat"> The details format. </param>
        /// <param name="args">          A variable-length parameters list containing arguments. </param>
        public virtual void OnEngineFailed( string detailsFormat, params object[] args )
        {
            this.OnEngineFailed( new System.IO.ErrorEventArgs( new EngineException( this.StateMachineInfo, detailsFormat, args ) ) );
        }

        #endregion

        #region " ENGINE FAILURE NOTIFIER "

        /// <summary> Gets or sets the on engine failed notifier. </summary>
        /// <value> The on engine failed event. </value>
        private ActionNotifier<System.IO.ErrorEventArgs> EngineFailedNotifier { get; set; }

        /// <summary>
        /// Registers a callback that will be invoked every time the state machine receives an engine
        /// failed notification.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="engineFailedAction"> The action to execute, accepting the details of the event
        /// arguments. </param>
        public void RegisterEngineFailureAction( Action<System.IO.ErrorEventArgs> engineFailedAction )
        {
            if ( engineFailedAction is null )
            {
                throw new ArgumentNullException( nameof( engineFailedAction ) );
            }

            this.EngineFailedNotifier.Register( engineFailedAction );
        }

        /// <summary> Unregisters the engine failure action described by engineFailedAction. </summary>
        /// <remarks> David, 2020-08-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="engineFailedAction"> The engine failed action. </param>
        public void UnregisterEngineFailureAction( Action<System.IO.ErrorEventArgs> engineFailedAction )
        {
            if ( engineFailedAction is null )
            {
                throw new ArgumentNullException( nameof( engineFailedAction ) );
            }

            this.EngineFailedNotifier.Unregister( engineFailedAction );
        }

        #endregion

        #region " FIRE ASYNC "

        /// <summary> Gets or sets the using fire asynchronous. </summary>
        /// <value> The using fire asynchronous. </value>
        public bool UsingFireAsync { get; set; }

        /// <summary> Fires the given trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="trigger"> The trigger. </param>
        public void Fire( TTrigger trigger )
        {
            if ( this.UsingFireAsync )
            {
                _ = this.StateMachine.FireAsync( trigger );
            }
            else
            {
                this.StateMachine.Fire( trigger );
            }
        }

        #endregion

    }
}
