using System;

namespace isr.Automata.Finite
{

    /// <summary> A state machine exception. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2020-08-26 </para>
    /// </remarks>
    [Serializable()]
    public class EngineException : System.Exception
    {

        #region " STANDARD CONSTRUCTORS "

        /// <summary> Gets or sets the default message. </summary>
        /// <value> The default message. </value>
        private static string DefaultMessage { get; set; } = "State machine engine exception";

        /// <summary> Initializes a new instance of the <see cref="EngineException"/> class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public EngineException() : this( DefaultMessage )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="EngineException" /> class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="message"> The message. </param>
        public EngineException( string message ) : base( message )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="EngineException" /> class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        public EngineException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary> Initializes a new instance of the class with serialized data. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        /// that holds the serialized object data about the exception being
        /// thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        /// that contains contextual information about the source or destination.
        /// </param>
        protected EngineException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
            if ( info is null )
            {
                return;
            }

            this.EngineErrorInfo = ( StateMachineInfo ) info.GetValue( $"{nameof( StateMachineInfo )}", typeof( StateMachineInfo ) );
        }

        /// <summary>
        /// Overrides the <see cref="GetObjectData" /> method to serialize custom values.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="info">    The <see cref="System.Runtime.Serialization.SerializationInfo">serialization
        /// information</see>. </param>
        /// <param name="context"> The <see cref="System.Runtime.Serialization.StreamingContext">streaming
        /// context</see> for the exception. </param>
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            if ( info is null )
            {
                return;
            }

            info.AddValue( $"{nameof( StateMachineInfo )}", this.EngineErrorInfo );
            base.GetObjectData( info, context );
        }


        #endregion

        #region " CUSTOM CONSTRUCTORS "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="enginerErrorInfo"> Information describing the enginer error. </param>
        public EngineException( StateMachineInfo enginerErrorInfo ) : this( DefaultMessage )
        {
            this.EngineErrorInfo = enginerErrorInfo;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="message">         The message. </param>
        /// <param name="engineErrorInfo"> Information describing the engine error. </param>
        public EngineException( string message, StateMachineInfo engineErrorInfo ) : this( message )
        {
            this.EngineErrorInfo = engineErrorInfo;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="engineErrorInfo"> Information describing the engine error. </param>
        /// <param name="format">          Describes the format to use. </param>
        /// <param name="args">            A variable-length parameters list containing arguments. </param>
        public EngineException( StateMachineInfo engineErrorInfo, string format, params object[] args ) : this( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), engineErrorInfo )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="engineErrorInfo"> Information describing the engine error. </param>
        /// <param name="innerException">  The inner exception. </param>
        public EngineException( StateMachineInfo engineErrorInfo, Exception innerException ) : this( DefaultMessage, engineErrorInfo, innerException )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="message">         The message. </param>
        /// <param name="engineErrorInfo"> Information describing the engine error. </param>
        /// <param name="innerException">  The inner exception. </param>
        public EngineException( string message, StateMachineInfo engineErrorInfo, Exception innerException ) : this( message, innerException )
        {
            this.EngineErrorInfo = engineErrorInfo;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-08-27. </remarks>
        /// <param name="engineErrorInfo"> Information describing the engine error. </param>
        /// <param name="innerException">  The inner exception. </param>
        /// <param name="format">          Describes the format to use. </param>
        /// <param name="args">            A variable-length parameters list containing arguments. </param>
        public EngineException( StateMachineInfo engineErrorInfo, Exception innerException, string format, params object[] args ) : this( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), engineErrorInfo, innerException )
        {
        }

        #endregion

        #region " ENGINE ERROR INFORMATION "

        /// <summary> Gets or sets information describing the engine error. </summary>
        /// <value> Information describing the engine error. </value>
        public StateMachineInfo EngineErrorInfo { get; private set; }

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> A String that represents this object. </returns>
        public override string ToString()
        {
            return this.EngineErrorInfo is null ? base.ToString() : this.EngineErrorInfo.ToString();
        }

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="exception"> The exception receiving the added data. </param>
        public void AddExceptionData( Exception exception )
        {
            if ( exception is null )
            {
                return;
            }

            this.EngineErrorInfo.AddExceptionData( exception );
        }


        #endregion

    }
}
