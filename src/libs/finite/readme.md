# About

isr.Automata.Finite is a .Net library supporting finite state machine automata .

# How to Use
This example is taken from the On/Off Engine state machine for the Engines project:
```
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a On/Off process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class OnOffEngine : EngineBase<OnOffState, OnOffTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> The initial state. </param>
        public OnOffEngine( string name, OnOffState initialState ) : base( name, new StateMachine<OnOffState, OnOffTrigger>( initialState ) )
        {
            StateMachine<OnOffState, OnOffTrigger>.StateConfiguration stateConfiguration;

            this.EngagedStates.Add( OnOffState.Off );
            stateConfiguration = this.StateMachine.Configure( OnOffState.Off );
            _ = stateConfiguration.PermitReentry( OnOffTrigger.Off );
            _ = stateConfiguration.Permit( OnOffTrigger.On, OnOffState.On );

            this.EngagedStates.Add( OnOffState.On );
            stateConfiguration = this.StateMachine.Configure( OnOffState.On );
            _ = stateConfiguration.Permit( OnOffTrigger.Off, OnOffState.Off );
            _ = stateConfiguration.PermitReentry( OnOffTrigger.On );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public OnOffEngine( string name ) : this( name, OnOffState.Off )
        {
        }

        #endregion

        #region " COMMANDS "

        /// <summary>   Move to or reenter the On state. </summary>
        /// <remarks>   David, 2020-10-01. </remarks>
        public void On()
        {
            this.Fire( OnOffTrigger.On );
        }

        /// <summary>   Move to or reenter the Off state. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Off()
        {
            this.Fire( OnOffTrigger.Off );
        }

        #endregion

    }

    /// <summary> Values that represent on off states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum OnOffState
    {

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off,

        /// <summary> An enum constant representing the on option. </summary>
        [Description( "On" )]
        On

    }

    /// <summary> Values that represent on off triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum OnOffTrigger
    {

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off,

        /// <summary> An enum constant representing the on option. </summary>
        [Description( "On" )]
        On
    }
}
```

# Key Features

* Encapsulates a generic Stateless state machine adding state 
machine information, special states, stopping functionality, failure
notifications and asynchronous triggering.

# Main Types

The main types provided by this library are:

* _ActionNotifier_ A thread-safe action notifier.
* _EngineBase_ A State Machine Engine base class.
* _EngineException_ A state machine exception.
* _ExceptionExtensions_ to be defined.
* _StateMachineInfo_ Provides information on a state machine.

# Feedback

isr.Automata.Finite is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Automata Repository].

[Automata Repository]: https://bitbucket.org/davidhary/dn.automata

