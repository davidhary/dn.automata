using System;

namespace isr.Automata.Finite.Engines.ExceptionExtensions
{

    /// <summary>
    /// Exception methods for adding exception data and building a detailed exception message.
    /// </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class ExceptionExtensionMethods
    {

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( Exception exception )
        {
            return Finite.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( exception );
        }

        /// <summary>   An Exception extension method that builds a message. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        /// <param name="exception">    The exception. </param>
        /// <returns>   A string. </returns>
        public static string BuildMessage( this Exception exception )
        {
            return Finite.ExceptionExtensions.ExceptionExtensionMethods.BuildMessage( exception );
        }
    }
}
