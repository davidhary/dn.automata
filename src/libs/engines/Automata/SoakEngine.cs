using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for the soak process. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-04-23, 1.0.5956.x. </para>
    /// </remarks>
    public class SoakEngine : EngineBase<SoakState, SoakTrigger>
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public SoakEngine( string name ) : base( name, new StateMachine<SoakState, SoakTrigger>( SoakState.Idle ) )
        {

            // start the sampling interval stopwatch.
            this.SampleIntervalStopwatch = Stopwatch.StartNew();
            this.SoakStopwatch = new Stopwatch();
            this.RampStopwatch = new Stopwatch();
            this.OffStateStopwatch = new Stopwatch();
            StateMachine<SoakState, SoakTrigger>.StateConfiguration stateConfiguration;

            stateConfiguration = this.StateMachine.Configure( SoakState.Idle );
            _ = stateConfiguration.PermitReentry( SoakTrigger.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Ramp );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );
            // activation actions are executed after the state entry actions and whenever the state Activate function is called.
            // .OnActivate(Sub() Me.OnActivate())

            stateConfiguration = this.StateMachine.Configure( SoakState.Engaged );
            _ = stateConfiguration.PermitReentry( SoakTrigger.Engage );
            _ = stateConfiguration.Permit( SoakTrigger.HasSetpoint, SoakState.HasSetpoint );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Ramp );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.HasSetpoint );
            _ = stateConfiguration.PermitReentry( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Permit( SoakTrigger.Ramp, SoakState.Ramp );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.Ramp );
            _ = stateConfiguration.PermitReentry( SoakTrigger.Ramp );
            _ = stateConfiguration.Permit( SoakTrigger.HasSetpoint, SoakState.HasSetpoint );
            _ = stateConfiguration.Permit( SoakTrigger.Timeout, SoakState.Timeout );
            _ = stateConfiguration.Permit( SoakTrigger.Soak, SoakState.Soak );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.OffSoak );
            _ = stateConfiguration.PermitReentry( SoakTrigger.OffSoak );
            _ = stateConfiguration.Permit( SoakTrigger.Ramp, SoakState.Ramp );
            _ = stateConfiguration.Permit( SoakTrigger.Soak, SoakState.Soak );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.Soak );
            _ = stateConfiguration.PermitReentry( SoakTrigger.Soak );
            _ = stateConfiguration.Permit( SoakTrigger.Timeout, SoakState.Timeout );
            _ = stateConfiguration.Permit( SoakTrigger.OffSoak, SoakState.OffSoak );
            _ = stateConfiguration.Permit( SoakTrigger.AtTemp, SoakState.AtTemp );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.OffTemp );
            _ = stateConfiguration.PermitReentry( SoakTrigger.OffTemp );
            _ = stateConfiguration.Permit( SoakTrigger.Ramp, SoakState.Ramp );
            _ = stateConfiguration.Permit( SoakTrigger.AtTemp, SoakState.AtTemp );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.AtTemp );
            _ = stateConfiguration.PermitReentry( SoakTrigger.AtTemp );
            _ = stateConfiguration.Permit( SoakTrigger.OffTemp, SoakState.OffTemp );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.Ramp );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.Timeout );
            _ = stateConfiguration.PermitReentry( SoakTrigger.Timeout );
            _ = stateConfiguration.Permit( SoakTrigger.HasSetpoint, SoakState.HasSetpoint );
            _ = stateConfiguration.Permit( SoakTrigger.Ramp, SoakState.Ramp );
            _ = stateConfiguration.Permit( SoakTrigger.Soak, SoakState.Soak );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Permit( SoakTrigger.Terminate, SoakState.Terminal );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Idle );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );

            stateConfiguration = this.StateMachine.Configure( SoakState.Terminal );
            _ = stateConfiguration.PermitReentry( SoakTrigger.Terminate );
            _ = stateConfiguration.Permit( SoakTrigger.Idle, SoakState.Idle );
            _ = stateConfiguration.Permit( SoakTrigger.Engage, SoakState.Engaged );
            _ = stateConfiguration.Ignore( SoakTrigger.AtTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.HasSetpoint );
            _ = stateConfiguration.Ignore( SoakTrigger.OffSoak );
            _ = stateConfiguration.Ignore( SoakTrigger.OffTemp );
            _ = stateConfiguration.Ignore( SoakTrigger.Ramp );
            _ = stateConfiguration.Ignore( SoakTrigger.Soak );
            _ = stateConfiguration.Ignore( SoakTrigger.Timeout );
            _ = stateConfiguration.OnEntry( t => this.OnEntryAction( t ) );
        }

        #endregion

        #region " STATE TRANSITION HANDLERS "

        /// <summary> Executes the entry action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnEntryAction( StateMachine<SoakState, SoakTrigger>.Transition transition )
        {
            if ( transition is null )
            {
                return;
            }

            switch ( transition.Destination )
            {
                case SoakState.Idle:
                    {
                        this.ProcessTimer?.Stop();
                        break;
                    }

                case SoakState.Engaged:
                    {
                        this.ProcessTimer?.Start();
                        break;
                    }

                case SoakState.HasSetpoint:
                    {
                        break;
                    }

                case SoakState.Ramp:
                    {
                        if ( !transition.IsReentry )
                        {
                            this.SoakStopwatch.Reset();
                        }

                        break;
                    }

                case SoakState.Soak:
                    {
                        if ( transition.IsReentry )
                        {
                            this.SoakCountElapsed += 1;
                        }
                        else
                        {
                            this.SoakCountElapsed = 1;
                            this.SoakStopwatch.Restart();
                            this.OffStateStopwatch.Reset();
                        }
                        this.SoakPercentProgress = this.SoakPercentProgressInternal;

                        break;
                    }

                case SoakState.OffSoak:
                    {
                        if ( transition.IsReentry )
                        {
                            this.OffStateCountElapsed += 1;
                        }
                        else
                        {
                            this.OffStateCountElapsed = 1;
                            this.OffStateStopwatch.Restart();
                        }

                        break;
                    }

                case SoakState.AtTemp:
                    {
                        if ( !transition.IsReentry )
                        {
                            this.OffStateStopwatch.Reset();
                        }

                        break;
                    }

                case SoakState.OffTemp:
                    {
                        if ( transition.IsReentry )
                        {
                            this.OffStateCountElapsed += 1;
                        }
                        else
                        {
                            this.OffStateCountElapsed = 1;
                            this.OffStateStopwatch.Restart();
                        }

                        break;
                    }

                case SoakState.Timeout:
                    {
                        this.Fire( SoakTrigger.Engage );
                        this.Fire( SoakTrigger.HasSetpoint );
                        break;
                    }

                case SoakState.Terminal:
                    {
                        this.ProcessTimer?.Stop();
                        break;
                    }

            }

            this.NotifyPropertyChanged( nameof( SoakEngine.StateProgress ) );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Terminates this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Terminate()
        {
            this.Fire( SoakTrigger.Terminate );
        }

        /// <summary> Idles (disconnects) the automaton. </summary>
        /// <remarks> Fires the idle trigger when the controller is disconnected. </remarks>
        public void Disengage()
        {
            this.Fire( SoakTrigger.Idle );
        }

        /// <summary> Engages (connects) the automaton. </summary>
        /// <remarks> Fires the Engage trigger when the controller is connected. </remarks>
        public void Engage()
        {
            this.Fire( SoakTrigger.Engage );
        }

        /// <summary>   Enables the automaton timer and engages (connects) the automaton . </summary>
        /// <remarks>   Fires the Engage trigger when the controller is connected. </remarks>
        /// <param name="temperatureSampleInterval">    The temperature sample interval. </param>
        public void Engage( TimeSpan temperatureSampleInterval )
        {
            this.ProcessTimer = new System.Timers.Timer {
                Interval = temperatureSampleInterval.TotalMilliseconds
            };
            this.ProcessTimer.Elapsed += this.ProcessTimer_Elapsed;
            this.Fire( SoakTrigger.Engage );
        }


        /// <summary> Applies the setpoint described by value. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        public void ApplySetpoint( double value )
        {
            if ( this.CurrentState == SoakState.Engaged )
            {
                this.Setpoint = value;
                this.Fire( SoakTrigger.HasSetpoint );
            }
            else
            {
                this.ChangeSetpoint( value );
            }
        }

        /// <summary> Change setpoint. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="value"> The value. </param>
        public void ChangeSetpoint( double value )
        {
            switch ( this.CurrentState )
            {
                case SoakState.Idle:
                    {
                        throw new InvalidOperationException( $"Failed changing setpoint because the state machine is {SoakState.Idle}; the controller must be connected first." );
                    }

                case SoakState.AtTemp:
                    {
                        this.Fire( SoakTrigger.OffTemp );
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.Terminal:
                case SoakState.Timeout:
                    {
                        this.Fire( SoakTrigger.Engage );
                        this.Fire( SoakTrigger.HasSetpoint );
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.Engaged:
                    {
                        this.Fire( SoakTrigger.HasSetpoint );
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.OffSoak:
                case SoakState.OffTemp:
                    {
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.HasSetpoint:
                    {
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.Ramp:
                    {
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.Soak:
                    {
                        this.Fire( SoakTrigger.OffSoak );
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                default:
                    {
                        throw new InvalidOperationException( $"Failed changing setpoint due to an unhandled state {this.CurrentState}" );
                    }
            }

            this.Setpoint = value;
        }

        /// <summary>
        /// Applies the temperature described by value until the state machine settles at a new state.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="value"> The value. </param>
        public void ApplyTemperature( double value )
        {
            var trigger = this.QueryNextTrigger( value );
            if ( trigger == SoakTrigger.Idle )
            {
                throw new InvalidOperationException( $"Failed changing temperature because the state machine is {SoakState.Idle}; the controller must be connected first." );
            }
            else
            {
                if ( this.IsTimerControlled() )
                { this.Fire( trigger ); }
                else
                { this.ApplyTemperature( trigger ); }
            }
        }

        /// <summary> Queries next trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="temperature"> The temperature. </param>
        /// <returns> The next trigger. </returns>
        private SoakTrigger QueryNextTrigger( double temperature )
        {
            var trigger = SoakTrigger.Engage;
            switch ( this.CurrentState )
            {
                case SoakState.Idle:
                    {
                        trigger = SoakTrigger.Idle;
                        break;
                    }

                case SoakState.Terminal:
                case SoakState.Timeout:
                    {
                        this.Fire( SoakTrigger.Engage );
                        this.Fire( SoakTrigger.HasSetpoint );
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.Engaged:
                    {
                        this.Fire( SoakTrigger.HasSetpoint );
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.HasSetpoint:
                    {
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }
            }

            this.Temperature = temperature;
            switch ( this.CurrentState )
            {
                case SoakState.Ramp:
                    {
                        // Timeout: Moves to ramp timeout state and display a pop-up dialog.
                        trigger = this.IsAtTemp() ? SoakTrigger.Soak : this.IsRampTimeout() ? SoakTrigger.Timeout : SoakTrigger.Ramp;

                        break;
                    }

                case SoakState.Soak:
                    {
                        trigger = this.IsSoakElapsed() ? SoakTrigger.AtTemp : this.IsOffTemp() ? SoakTrigger.OffSoak : SoakTrigger.Soak;

                        break;
                    }

                case SoakState.OffSoak:
                    {
                        trigger = this.IsAtTemp() ? SoakTrigger.Soak : this.IsOffStateDurationElapsed() ? SoakTrigger.Ramp : SoakTrigger.OffSoak;

                        break;
                    }

                case SoakState.AtTemp:
                    {
                        trigger = this.IsOffTemp() ? SoakTrigger.OffTemp : SoakTrigger.AtTemp;
                        break;
                    }

                case SoakState.OffTemp:
                    {
                        trigger = this.IsAtTemp() ? SoakTrigger.AtTemp : this.IsOffStateDurationElapsed() ? SoakTrigger.Ramp : SoakTrigger.OffTemp;

                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled case" );
                        break;
                    }
            }

            return trigger;
        }

        /// <summary>
        /// Applies the temperature described by value until the state machine settles at a new state.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="trigger"> The trigger. </param>
        private void ApplyTemperature( SoakTrigger trigger )
        {
            this.Fire( trigger );
            switch ( this.CurrentState )
            {
                case SoakState.Ramp:
                    {
                        if ( this.IsAtTemp() )
                        {
                            trigger = SoakTrigger.Soak;
                            this.ApplyTemperature( trigger );
                        }
                        else if ( this.IsRampTimeout() )
                        {
                            // Move to ramp timeout state and display a pop-up dialog.
                            trigger = SoakTrigger.Timeout;
                            this.Fire( trigger );
                        }

                        break;
                    }

                case SoakState.Soak:
                    {
                        if ( this.IsSoakElapsed() )
                        {
                            trigger = SoakTrigger.AtTemp;
                            this.ApplyTemperature( trigger );
                        }
                        else if ( this.IsOffTemp() )
                        {
                            trigger = SoakTrigger.OffSoak;
                            this.Fire( trigger );
                        }

                        break;
                    }

                case SoakState.OffSoak:
                    {
                        if ( this.IsAtTemp() )
                        {
                            trigger = SoakTrigger.Soak;
                            this.ApplyTemperature( trigger );
                        }
                        else if ( this.IsOffStateDurationElapsed() )
                        {
                            trigger = SoakTrigger.Ramp;
                            this.ApplyTemperature( trigger );
                        }

                        break;
                    }

                case SoakState.AtTemp:
                    {
                        if ( this.IsOffTemp() )
                        {
                            trigger = SoakTrigger.OffTemp;
                            this.ApplyTemperature( trigger );
                        }

                        break;
                    }

                case SoakState.OffTemp:
                    {
                        if ( this.IsAtTemp() )
                        {
                            trigger = SoakTrigger.AtTemp;
                            this.ApplyTemperature( trigger );
                        }
                        else if ( this.IsOffStateDurationElapsed() )
                        {
                            trigger = SoakTrigger.Ramp;
                            this.ApplyTemperature( trigger );
                        }

                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        /// <summary>   Query state machine progress by reentering the active state. </summary>
        /// <remarks>   David, 2020-11-24. </remarks>
        public void QueryMachineProgress()
        {
            switch ( this.CurrentState )
            {
                case SoakState.Engaged:
                    {
                        this.Fire( SoakTrigger.Engage );
                        break;
                    }

                case SoakState.HasSetpoint:
                    {
                        this.Fire( SoakTrigger.HasSetpoint );
                        break;
                    }

                case SoakState.Ramp:
                    {
                        this.Fire( SoakTrigger.Ramp );
                        break;
                    }

                case SoakState.Soak:
                    {
                        this.Fire( SoakTrigger.Soak );
                        break;
                    }

                case SoakState.OffSoak:
                    {
                        this.Fire( SoakTrigger.OffSoak );
                        break;
                    }

                case SoakState.AtTemp:
                    {
                        this.Fire( SoakTrigger.AtTemp );
                        break;
                    }

                case SoakState.OffTemp:
                    {
                        this.Fire( SoakTrigger.OffTemp );
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        #endregion

        #region " STATE AUTOMATION CONDITIONS "

        /// <summary> The temperature. </summary>
        private double _Temperature;

        /// <summary> Gets or sets the temperature. </summary>
        /// <value> The temperature. </value>
        public double Temperature
        {
            get => this._Temperature;

            set {
                if ( value != this.Temperature )
                {
                    this._Temperature = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The temperature resolution. </summary>
        private double _TemperatureResolution;

        /// <summary> Gets or sets the temperature resolution. </summary>
        /// <value> The temperature resolution. </value>
        public double TemperatureResolution
        {
            get => this._TemperatureResolution;

            set {
                if ( value != this.TemperatureResolution )
                {
                    this._TemperatureResolution = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The setpoint. </summary>
        private double _Setpoint;

        /// <summary> Gets or sets the setpoint. </summary>
        /// <value> The setpoint. </value>
        public double Setpoint
        {
            get => this._Setpoint;

            set {
                if ( value != this.Setpoint )
                {
                    this._Setpoint = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The window. </summary>
        private double _Window;

        /// <summary> Gets or sets the window. </summary>
        /// <value> The window. </value>
        public double Window
        {
            get => this._Window;

            set {
                if ( value != this.Window )
                {
                    this._Window = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The ramp timeout. </summary>
        private TimeSpan _RampTimeout;

        /// <summary> Gets or sets the ramp timeout. </summary>
        /// <value> The ramp timeout. </value>
        public TimeSpan RampTimeout
        {
            get => this._RampTimeout;

            set {
                if ( value != this.RampTimeout )
                {
                    this._RampTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The hysteresis. </summary>
        private double _Hysteresis;

        /// <summary> Gets or sets the Hysteresis. </summary>
        /// <value> The Hysteresis. </value>
        public double Hysteresis
        {
            get => this._Hysteresis;

            set {
                if ( value != this.Hysteresis )
                {
                    this._Hysteresis = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The soak count elapsed. </summary>
        private int _SoakCountElapsed;

        /// <summary> Gets or sets the number of counts elapsed during the soak. </summary>
        /// <value> The Soak Count Elapsed. </value>
        public int SoakCountElapsed
        {
            get => this._SoakCountElapsed;

            set {
                if ( value != this.SoakCountElapsed )
                {
                    this._SoakCountElapsed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Number of soaks. </summary>
        private int _SoakCount;

        /// <summary> Gets or sets the number of times the soak was queried. </summary>
        /// <remarks>
        /// Soak lasts either for a <see cref="SoakDuration"/> specified time or <see cref="SoakCount"/>
        /// specified number of readings.
        /// </remarks>
        /// <value> The Soak Duration Count. </value>
        public int SoakCount
        {
            get => this._SoakCount;

            set {
                if ( value != this.SoakCount )
                {
                    this._SoakCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Duration of the soak. </summary>
        private TimeSpan _SoakDuration;

        /// <summary> Gets or sets the duration of the soak. </summary>
        /// <value> The soak duration. </value>
        public TimeSpan SoakDuration
        {
            get => this._SoakDuration;

            set {
                if ( value != this.SoakDuration )
                {
                    this._SoakDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The off state count elapsed. </summary>
        private int _OffStateCountElapsed;

        /// <summary>
        /// Gets or sets the number of sample times elapsed during either the
        /// <see cref="SoakState.OffSoak"/> or
        /// <see cref="SoakState.OffTemp"/> states .
        /// </summary>
        /// <value> The OffState Duration Count Elapsed. </value>
        public int OffStateCountElapsed
        {
            get => this._OffStateCountElapsed;

            set {
                if ( value != this.OffStateCountElapsed )
                {
                    this._OffStateCountElapsed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Number of allowed off state samples. </summary>
        private int _AllowedOffStateCount;

        /// <summary>
        /// Gets or sets the number of sample times allowed in either the <see cref="SoakState.OffSoak"/>
        /// or
        /// <see cref="SoakState.OffTemp"/> states before triggering back to the
        /// <see cref="SoakState.Ramp"/> state.
        /// </summary>
        /// <value> The allowed off-state Count. </value>
        public int AllowedOffStateCount
        {
            get => this._AllowedOffStateCount;

            set {
                if ( value != this.AllowedOffStateCount )
                {
                    this._AllowedOffStateCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Duration of the allowed off state. </summary>
        private TimeSpan _AllowedOffStateDuration;

        /// <summary>
        /// Gets or sets the time allowed in either the <see cref="SoakState.OffSoak"/> or
        /// <see cref="SoakState.OffTemp"/> states before triggering back to the
        /// <see cref="SoakState.Ramp"/> state.
        /// </summary>
        /// <value> The allowed off-state duration. </value>
        public TimeSpan AllowedOffStateDuration
        {
            get => this._AllowedOffStateDuration;

            set {
                if ( value != this.AllowedOffStateDuration )
                {
                    this._AllowedOffStateDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The sample interval. </summary>
        private TimeSpan _SampleInterval;

        /// <summary> Gets or sets the sample interval. </summary>
        /// <value> The sample interval. </value>
        public TimeSpan SampleInterval
        {
            get => this._SampleInterval;

            set {
                if ( value != this.SampleInterval )
                {
                    this._SampleInterval = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Query if  <paramref name="value"/> is within <see cref="TemperatureResolution"/> of the
        /// <see cref="Setpoint"/>.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if at setpoint; otherwise <c>false</c> </returns>
        public bool IsAtSetpoint( double value )
        {
            return Math.Abs( value - this.Setpoint ) <= this.TemperatureResolution;
        }

        /// <summary> Query if the soak automation is at temp and staying at temp. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns>
        /// <c>true</c> if the soak automation is at temp and staying at temp; otherwise <c>false</c>
        /// </returns>
        public bool IsStayingAtTemp()
        {
            var nextSymbol = this.QueryNextTrigger( this.Temperature );
            return this.LastTransition.Destination == SoakState.AtTemp && (SoakTrigger.AtTemp == nextSymbol || this.LastTransition.Trigger == nextSymbol);
        }

        #endregion

        #region " STATE AUTOMATION PROGRESS "

        /// <summary>
        /// Query if  <see cref="Temperature"/> is within <see cref="Window"/> of the
        /// <see cref="Setpoint"/>.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if at temperature; otherwise <c>false</c> </returns>
        public bool IsAtTemp()
        {
            return Math.Abs( this.Temperature - this.Setpoint ) <= 0.5d * this.Window;
        }

        /// <summary>
        /// Query if  <see cref="Temperature"/> is outside <see cref="Window"/>+<see cref="Hysteresis"/>
        /// of the <see cref="Setpoint"/>.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if off temporary; otherwise <c>false</c> </returns>
        public bool IsOffTemp()
        {
            return Math.Abs( this.Temperature - this.Setpoint ) > 0.5d * this.Window + this.Hysteresis;
        }

        /// <summary> Gets the soak percent progress. </summary>
        /// <value> The soak progress. </value>
        private int SoakPercentProgressInternal
        {
            get {
                if ( this.SoakDuration > TimeSpan.Zero && (this.CurrentState == SoakState.Soak || this.CurrentState == SoakState.OffSoak) )
                {
                    int value = ( int ) (100d * this.SoakStopwatch.Elapsed.TotalSeconds / this.SoakDuration.TotalSeconds);
                    return value < 0 ? 0 : value > 100 ? 100 : value;
                }
                else
                {
                    return this.StateProgress;
                }
            }
        }

        /// <summary> Gets the state progress. </summary>
        /// <value> The state progress. </value>
        public int StateProgress => ( int ) (100 * ( int ) this.CurrentState / ( double ) SoakState.Terminal);

        /// <summary> The Soak percent progress. </summary>
        private int _SoakPercentProgress;

        /// <summary> The Soak percent progress. </summary>
        /// <value> The Soak percent progress. </value>
        public int SoakPercentProgress
        {
            get => this._SoakPercentProgress;

            set {
                value = Math.Min( 100, Math.Max( 0, value ) );
                if ( value != this.SoakPercentProgress )
                {
                    this._SoakPercentProgress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the ramp stopwatch. </summary>
        /// <value> The ramp stopwatch. </value>
        public Stopwatch RampStopwatch { get; private set; }

        /// <summary> Query if this object is ramp timeout. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if ramp timeout; otherwise <c>false</c> </returns>
        public bool IsRampTimeout()
        {
            return this.RampTimeout > TimeSpan.Zero && this.RampStopwatch.Elapsed > this.RampTimeout;
        }

        /// <summary> Gets or sets the soak stopwatch. </summary>
        /// <value> The soak stopwatch. </value>
        public Stopwatch SoakStopwatch { get; private set; }

        /// <summary> Query if the soak time or count elapsed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if soak elapsed; otherwise <c>false</c> </returns>
        public bool IsSoakElapsed()
        {
            return this.SoakCount == 0 && this.SoakDuration == TimeSpan.Zero ||
                   this.SoakDuration > TimeSpan.Zero && this.SoakStopwatch.Elapsed > this.SoakDuration ||
                   this.SoakCount > 0 && this.SoakCountElapsed >= this.SoakCount;
        }

        /// <summary> Gets or sets the sample interval stopwatch. </summary>
        /// <remarks> This stopwatch runs continuously. </remarks>
        /// <value> The sample interval stopwatch. </value>
        public Stopwatch SampleIntervalStopwatch { get; private set; }

        /// <summary> Executes the sample handled action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void OnSampleHandled()
        {
            this.SampleIntervalStopwatch.Restart();
        }

        /// <summary> Query if the sample interval elapsed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if sample interval elapsed; otherwise <c>false</c> </returns>
        public bool IsSampleIntervalElapsed()
        {
            return this.IsSampleIntervalElapsed( this.SampleInterval );
        }

        /// <summary> Query if the sample interval elapsed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="interval"> The interval. </param>
        /// <returns> <c>true</c> if sample interval elapsed; otherwise <c>false</c> </returns>
        public bool IsSampleIntervalElapsed( TimeSpan interval )
        {
            return this.SampleIntervalStopwatch.Elapsed >= interval;
        }

        /// <summary> Gets or sets the 'off' state duration stopwatch. </summary>
        /// <value> The off' state duration stopwatch. </value>
        public Stopwatch OffStateStopwatch { get; private set; }

        /// <summary> Query if the allowed 'off' state duration elapsed. </summary>
        /// <remarks>
        /// The automata will resume from an off-soak to soak or off-temp to temp if the 'off' state
        /// lasts less than the allowed <see cref="AllowedOffStateDuration">duration</see> or
        /// <see cref="AllowedOffStateCount">count</see> .
        /// </remarks>
        /// <returns> <c>true</c> if 'off' state duration elapsed; otherwise <c>false</c> </returns>
        public bool IsOffStateDurationElapsed()
        {
            return this.AllowedOffStateCount == 0 && this.AllowedOffStateDuration == TimeSpan.Zero ||
                   this.AllowedOffStateDuration > TimeSpan.Zero && this.OffStateStopwatch.Elapsed > this.AllowedOffStateDuration ||
                   this.AllowedOffStateCount > 0 && this.OffStateCountElapsed >= this.AllowedOffStateCount;
        }

        /// <summary>
        /// Query if this object is active; that is is neither in the <see cref="SoakState.Idle"/> or
        /// <see cref="SoakState.Terminal"/> states.
        /// </summary>
        /// <remarks>   David, 2020-11-24. </remarks>
        /// <returns>   True if active, false if not. </returns>
        public bool IsActive()
        {
            return (SoakState.Idle != this.CurrentState) && (SoakState.Terminal != this.CurrentState);
        }


        #endregion

        #region " PROCESS TIMER "

        /// <summary>
        /// Gets or sets the process timer, which events are used to query the soak engine progress.
        /// </summary>
        /// <value> The process timer. </value>
        private System.Timers.Timer ProcessTimer { get; set; }

        /// <summary>
        /// Event handler. Called by <see cref="ProcessTimer"/> for elapsed events. Re-enters active
        /// state to request a setpoint or oven controls status or to take a temperature reading.
        /// </summary>
        /// <remarks>   David, 2020-11-24. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Elapsed event information. </param>
        private void ProcessTimer_Elapsed( object sender, ElapsedEventArgs e )
        {
            // the process timer must be resumed by the process handler.
            this.ProcessTimer.Stop();
            this.QueryMachineProgress();
        }

        /// <summary>   Query if the soak engine is running under timer control. </summary>
        /// <remarks>   David, 2020-11-24. </remarks>
        /// <returns>   True if running, false if not. </returns>
        public bool IsTimerControlled()
        {
            return this.ProcessTimer is Object;
        }

        /// <summary> Pauses the process timer. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        public void Pause()
        {
            this.ProcessTimer.Stop();
        }

        /// <summary> Resumes the process timer. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public void Resume()
        {
            this.ProcessTimer.Start();
        }

        #endregion

    }

    /// <summary>
    /// Enumerates the state transition triggers for the
    /// <see cref="SoakEngine">soak</see> automaton.
    /// </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum SoakTrigger
    {

        /// <summary> An enum constant representing the idle option. </summary>
        [Description( "Idle (disconnect): Idle; " )]
        Idle,

        /// <summary> An enum constant representing the engage option. </summary>
        [Description( "Engage (connect): Engaged; " )]
        Engage,

        /// <summary> . </summary>
        [Description( "Ready: Initial -> Ready" )]
        HasSetpoint,

        /// <summary> . </summary>
        [Description( "Ramp: Ready -> Ramp" )]
        Ramp,

        /// <summary> . </summary>
        [Description( "Soak: Ramp, OffSoak -> Soak" )]
        Soak,

        /// <summary> . </summary>
        [Description( "OffSoak: Soak -> OffSoak" )]
        OffSoak,

        /// <summary> . </summary>
        [Description( "AtTemp: Soak; OffTemp -> AtTemp" )]
        AtTemp,

        /// <summary> . </summary>
        [Description( "OffTemp: AtTemp -> OffTemp" )]
        OffTemp,

        /// <summary> . </summary>
        [Description( "Timeout: Ramp; Soak -> Timeout" )]
        Timeout,

        /// <summary> . </summary>
        [Description( "Terminate (stop): Any state -> Terminal" )]
        Terminate
    }

    /// <summary> Values that represent Soak automaton states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum SoakState
    {

        /// <summary> An enum constant representing the idle option. </summary>
        [Description( "Idle" )]
        Idle,

        /// <summary> An enum constant representing the engaged option. </summary>
        [Description( "Engaged" )]
        Engaged,

        /// <summary> An enum constant representing the has setpoint option. </summary>
        [Description( "Setpoint" )]
        HasSetpoint,

        /// <summary> An enum constant representing the ramp option. </summary>
        [Description( "Ramp" )]
        Ramp,

        /// <summary> An enum constant representing the off soak option. </summary>
        [Description( "Off-Soak" )]
        OffSoak,

        /// <summary> An enum constant representing the soak option. </summary>
        [Description( "Soak" )]
        Soak,

        /// <summary> An enum constant representing the off Temporary option. </summary>
        [Description( "Off-Temp" )]
        OffTemp,

        /// <summary> An enum constant representing at Temporary option. </summary>
        [Description( "At-Temp" )]
        AtTemp,

        /// <summary> An enum constant representing the timeout option. </summary>
        [Description( "Timeout" )]
        Timeout,

        /// <summary> An enum constant representing the terminal option. </summary>
        [Description( "Terminal" )]
        Terminal
    }
}
