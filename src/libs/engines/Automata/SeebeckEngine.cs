using System;
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a soaking meter device. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para>
    /// </remarks>
    public class SeebeckEngine : EngineBase<SeebeckState, SeebeckTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public SeebeckEngine( string name ) : base( name, new StateMachine<SeebeckState, SeebeckTrigger>( SeebeckState.Initial ) )
        {
            StateMachine<SeebeckState, SeebeckTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Initial );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Initial );
            _ = stateConfiguration.Permit( SeebeckTrigger.Insert, SeebeckState.Inserting );
            _ = stateConfiguration.Permit( SeebeckTrigger.Soak, SeebeckState.Soaking );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Contact );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Measure );
            _ = stateConfiguration.Permit( SeebeckTrigger.Fail, SeebeckState.Failed );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Remove );
            this.EngagedStates.Add( SeebeckState.Inserting );
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Inserting );
            _ = stateConfiguration.Permit( SeebeckTrigger.Initial, SeebeckState.Initial );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Insert );
            _ = stateConfiguration.Permit( SeebeckTrigger.Contact, SeebeckState.Contacting );
            _ = stateConfiguration.Permit( SeebeckTrigger.Soak, SeebeckState.Soaking );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Measure );
            _ = stateConfiguration.Permit( SeebeckTrigger.Fail, SeebeckState.Failed );
            _ = stateConfiguration.Permit( SeebeckTrigger.Remove, SeebeckState.Removing );
            this.EngagedStates.Add( SeebeckState.Soaking );
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Soaking );
            _ = stateConfiguration.Permit( SeebeckTrigger.Initial, SeebeckState.Initial );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Insert );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Soak );
            _ = stateConfiguration.Permit( SeebeckTrigger.Contact, SeebeckState.Contacting );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Measure );
            _ = stateConfiguration.Permit( SeebeckTrigger.Fail, SeebeckState.Failed );
            _ = stateConfiguration.Permit( SeebeckTrigger.Remove, SeebeckState.Removing );
            this.EngagedStates.Add( SeebeckState.Contacting );
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Contacting );
            _ = stateConfiguration.Permit( SeebeckTrigger.Initial, SeebeckState.Initial );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Insert );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Soak );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Contact );
            _ = stateConfiguration.Permit( SeebeckTrigger.Measure, SeebeckState.Measuring );
            _ = stateConfiguration.Permit( SeebeckTrigger.Fail, SeebeckState.Failed );
            _ = stateConfiguration.Permit( SeebeckTrigger.Remove, SeebeckState.Removing );
            this.EngagedStates.Add( SeebeckState.Measuring );
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Measuring );
            _ = stateConfiguration.Permit( SeebeckTrigger.Initial, SeebeckState.Initial );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Insert );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Soak );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Contact );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Measure );
            _ = stateConfiguration.Permit( SeebeckTrigger.Fail, SeebeckState.Failed );
            _ = stateConfiguration.Permit( SeebeckTrigger.Remove, SeebeckState.Removing );
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Failed );
            _ = stateConfiguration.Permit( SeebeckTrigger.Initial, SeebeckState.Initial );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Insert );
            _ = stateConfiguration.Permit( SeebeckTrigger.Soak, SeebeckState.Soaking );
            // stateConfiguration.Ignore(SeebeckTrigger.Contact)
            // stateConfiguration.Permit(SeebeckTrigger.Measure, SeebeckState.Contacting)
            _ = stateConfiguration.Permit( SeebeckTrigger.Contact, SeebeckState.Contacting );
            _ = stateConfiguration.Permit( SeebeckTrigger.Measure, SeebeckState.Measuring );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Fail );
            _ = stateConfiguration.Permit( SeebeckTrigger.Remove, SeebeckState.Removing );
            stateConfiguration = this.StateMachine.Configure( SeebeckState.Removing );
            _ = stateConfiguration.Permit( SeebeckTrigger.Initial, SeebeckState.Initial );
            // stateConfiguration.Ignore(SeebeckTrigger.Insert)
            _ = stateConfiguration.Permit( SeebeckTrigger.Soak, SeebeckState.Soaking );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Contact );
            _ = stateConfiguration.Ignore( SeebeckTrigger.Measure );
            _ = stateConfiguration.Permit( SeebeckTrigger.Fail, SeebeckState.Failed );
            _ = stateConfiguration.PermitReentry( SeebeckTrigger.Remove );
        }

        #endregion

        #region " PROGRESS "

        /// <summary> Gets the state progress. </summary>
        /// <value> The state progress. </value>
        public int StateProgress => ( int ) (100 * ( int ) this.CurrentState / ( double ) SeebeckState.Removing);

        /// <summary> The percent progress. </summary>
        private int _PercentProgress;

        /// <summary> The percent progress. </summary>
        /// <value> The percent progress. </value>
        public int PercentProgress
        {
            get => this._PercentProgress;

            set {
                value = Math.Min( 100, Math.Max( 0, value ) );
                if ( value != this.PercentProgress )
                {
                    this._PercentProgress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Updates the percent progress. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UpdatePercentProgress()
        {
            if ( this.CurrentState != SeebeckState.Initial )
            {
                this.PercentProgress = this.StateProgress;
            }
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Process the stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void ProcessStopRequest()
        {
            if ( this.StoppingTimeoutElapsed() )
            {
                this.Fire( SeebeckTrigger.Initial );
            }
            else
            {
                switch ( this.CurrentState )
                {
                    case SeebeckState.Contacting:
                        {
                            this.Fire( SeebeckTrigger.Initial );
                            break;
                        }

                    case SeebeckState.Failed:
                        {
                            this.Fire( SeebeckTrigger.Initial );
                            break;
                        }

                    case SeebeckState.Initial:
                        {
                            this.Fire( SeebeckTrigger.Initial );
                            break;
                        }

                    case SeebeckState.Inserting:
                        {
                            this.Fire( SeebeckTrigger.Remove );
                            break;
                        }

                    case SeebeckState.Measuring:
                        {
                            this.Fire( SeebeckTrigger.Remove );
                            break;
                        }

                    case SeebeckState.Removing:
                        {
                            this.Fire( SeebeckTrigger.Initial );
                            break;
                        }

                    case SeebeckState.Soaking:
                        {
                            this.Fire( SeebeckTrigger.Remove );
                            break;
                        }

                    default:
                        {
                            throw new InvalidOperationException( $"Unhanded {this.StateMachine.State} state processing stop" );
                        }
                }
            }
        }

        /// <summary> Await part present. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void AwaitPartPresent()
        {
            this.Fire( SeebeckTrigger.Insert );
        }

        /// <summary> Part inserted; soak. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void SoakPart()
        {
            this.Fire( SeebeckTrigger.Soak );
        }

        /// <summary> Part removed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( SeebeckTrigger.Initial );
        }

        /// <summary> Part soaked; connect. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void ConnectPart()
        {
            this.Fire( SeebeckTrigger.Contact );
        }

        /// <summary> Part contacted, measure. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void MeasurePart()
        {
            this.Fire( SeebeckTrigger.Measure );
        }

        /// <summary> Part measured remove. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void RemovePart()
        {
            this.Fire( SeebeckTrigger.Remove );
        }

        /// <summary> Seebeck failed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Fail()
        {
            this.Fire( SeebeckTrigger.Fail );
        }

        #endregion

    }

    /// <summary> Values that represent Seebeck states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum SeebeckState
    {

        /// <summary> An enum constant representing the initial state; Part not present. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the initial state; Awaiting for part present. </summary>
        [Description( "Inserting" )]
        Inserting,

        /// <summary> An enum constant representing the soaking state; part present, temperature monitoring is active. </summary>
        [Description( "Soaking" )]
        Soaking,

        /// <summary> An enum constant representing the contacting state; part at temperature establishing contact. </summary>
        [Description( "Contacting" )]
        Contacting,

        /// <summary> An enum constant representing the measuring state; Measuring Seebeck voltage. </summary>
        [Description( "Measuring" )]
        Measuring,

        /// <summary> An enum constant representing the failed state. Measurement failed. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the removing state. Removing the part. </summary>
        [Description( "Removing" )]
        Removing
    }

    /// <summary> Values that represent Seebeck triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum SeebeckTrigger
    {

        /// <summary> An enum constant representing the initial trigger. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the insert trigger. Waiting for part present; go to the inserting state. </summary>
        [Description( "Insert" )]
        Insert,

        /// <summary> An enum constant representing the Soak trigger. Part inserted; start monitoring temperature. </summary>
        [Description( "Part inserted; Soak" )]
        Soak,

        /// <summary> An enum constant representing the Contact trigger command; part soaked, start establishing contact. </summary>
        [Description( "Contact" )]
        Contact,

        /// <summary> An enum constant representing the measure trigger; Part contact made, start measuring Seebeck voltage. </summary>
        [Description( "Measure" )]
        Measure,

        /// <summary> An enum constant representing the Remove trigger. Part measured or failed, remove the part. </summary>
        [Description( "Remove" )]
        Remove,

        /// <summary> An enum constant representing the fail option. Measurement failed. </summary>
        [Description( "Fail" )]
        Fail
    }
}
