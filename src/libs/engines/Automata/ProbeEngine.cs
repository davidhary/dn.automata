using System;
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a probe device. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para>
    /// </remarks>
    public class ProbeEngine : EngineBase<ProbeState, ProbeTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public ProbeEngine( string name ) : base( name, new StateMachine<ProbeState, ProbeTrigger>( ProbeState.Initial ) )
        {
            StateMachine<ProbeState, ProbeTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( ProbeState.Initial );
            _ = stateConfiguration.Permit( ProbeTrigger.Fail, ProbeState.Failed );
            _ = stateConfiguration.PermitReentry( ProbeTrigger.Initial );
            _ = stateConfiguration.Ignore( ProbeTrigger.Lift );
            _ = stateConfiguration.Permit( ProbeTrigger.Lower, ProbeState.Lowering );
            _ = stateConfiguration.Ignore( ProbeTrigger.Probe );
            this.EngagedStates.Add( ProbeState.Lowering );
            stateConfiguration = this.StateMachine.Configure( ProbeState.Lowering );
            _ = stateConfiguration.Permit( ProbeTrigger.Fail, ProbeState.Failed );
            _ = stateConfiguration.Permit( ProbeTrigger.Initial, ProbeState.Initial );
            _ = stateConfiguration.Ignore( ProbeTrigger.Lift );
            _ = stateConfiguration.PermitReentry( ProbeTrigger.Lower );
            _ = stateConfiguration.Permit( ProbeTrigger.Probe, ProbeState.Probing );
            this.EngagedStates.Add( ProbeState.Probing );
            stateConfiguration = this.StateMachine.Configure( ProbeState.Probing );
            _ = stateConfiguration.Permit( ProbeTrigger.Fail, ProbeState.Failed );
            _ = stateConfiguration.Permit( ProbeTrigger.Initial, ProbeState.Initial );
            _ = stateConfiguration.Permit( ProbeTrigger.Lift, ProbeState.Lifting );
            _ = stateConfiguration.Ignore( ProbeTrigger.Lower );
            _ = stateConfiguration.PermitReentry( ProbeTrigger.Probe );
            stateConfiguration = this.StateMachine.Configure( ProbeState.Failed );
            _ = stateConfiguration.Permit( ProbeTrigger.Initial, ProbeState.Initial );
            _ = stateConfiguration.Ignore( ProbeTrigger.Lift );
            _ = stateConfiguration.PermitReentry( ProbeTrigger.Fail );
            _ = stateConfiguration.Permit( ProbeTrigger.Lower, ProbeState.Lowering );
            _ = stateConfiguration.Ignore( ProbeTrigger.Probe );
            stateConfiguration = this.StateMachine.Configure( ProbeState.Lifting );
            _ = stateConfiguration.Permit( ProbeTrigger.Fail, ProbeState.Failed );
            _ = stateConfiguration.Permit( ProbeTrigger.Initial, ProbeState.Initial );
            _ = stateConfiguration.PermitReentry( ProbeTrigger.Lift );
            _ = stateConfiguration.Permit( ProbeTrigger.Lower, ProbeState.Lowering );
            _ = stateConfiguration.Ignore( ProbeTrigger.Probe );
        }

        #endregion

        #region " PROGRESS "

        /// <summary> Gets the state progress. </summary>
        /// <value> The state progress. </value>
        public int StateProgress => ( int ) (100 * ( int ) this.CurrentState / ( double ) ProbeState.Lifting);

        /// <summary> The percent progress. </summary>
        private int _PercentProgress;

        /// <summary> The percent progress. </summary>
        /// <value> The percent progress. </value>
        public int PercentProgress
        {
            get => this._PercentProgress;

            set {
                value = Math.Min( 100, Math.Max( 0, value ) );
                if ( value != this.PercentProgress )
                {
                    this._PercentProgress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Updates the percent progress. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UpdatePercentProgress()
        {
            if ( this.CurrentState != ( int ) SeebeckState.Initial )
            {
                this.PercentProgress = this.StateProgress;
            }
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Process the stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void ProcessStopRequest()
        {
            if ( this.StoppingTimeoutElapsed() )
            {
                this.Fire( ProbeTrigger.Initial );
            }
            else
            {
                switch ( this.CurrentState )
                {
                    case ProbeState.Failed:
                        {
                            this.Fire( ProbeTrigger.Initial );
                            break;
                        }

                    case ProbeState.Initial:
                        {
                            this.Fire( ProbeTrigger.Initial );
                            break;
                        }

                    case ProbeState.Lifting:
                        {
                            this.Fire( ProbeTrigger.Initial );
                            break;
                        }

                    case ProbeState.Lowering:
                        {
                            this.Fire( ProbeTrigger.Initial );
                            break;
                        }

                    case ProbeState.Probing:
                        {
                            this.Fire( ProbeTrigger.Lift );
                            break;
                        }

                    default:
                        {
                            throw new InvalidOperationException( $"Unhanded {this.StateMachine.State} state processing stop" );
                        }
                }
            }
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( ProbeTrigger.Initial );
        }

        /// <summary> Starts probe lifting. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Lift()
        {
            this.Fire( ProbeTrigger.Lift );
        }

        /// <summary> Starts probe lowering. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Lower()
        {
            this.Fire( ProbeTrigger.Lower );
        }

        /// <summary> Starts making measurements. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Probe()
        {
            this.Fire( ProbeTrigger.Probe );
        }

        /// <summary> Probe failed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Fail()
        {
            this.Fire( ProbeTrigger.Fail );
        }

        #endregion

    }

    /// <summary> Values that represent probe states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum ProbeState
    {

        /// <summary> An enum constant representing the initial state. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the lowering state. </summary>
        [Description( "Lowering" )]
        Lowering,

        /// <summary> An enum constant representing the probing state. </summary>
        [Description( "Probing" )]
        Probing,

        /// <summary> An enum constant representing the failed state. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the lifting state. </summary>
        [Description( "Lifting" )]
        Lifting
    }

    /// <summary> Values that represent probe triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum ProbeTrigger
    {

        /// <summary> An enum constant representing the initial trigger. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the lower probe trigger. </summary>
        [Description( "Lower" )]
        Lower,

        /// <summary> An enum constant representing the probe measure trigger. </summary>
        [Description( "Prob" )]
        Probe,

        /// <summary> An enum constant representing the fail probe trigger. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the lift probe trigger. </summary>
        [Description( "Lift" )]
        Lift
    }
}
