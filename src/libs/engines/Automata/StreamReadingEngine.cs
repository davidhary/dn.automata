using System;
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a stream reading device. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-08-20 </para>
    /// </remarks>
    public class StreamReadingEngine : EngineBase<StreamReadingState, StreamReadingTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public StreamReadingEngine( string name ) : base( name, new StateMachine<StreamReadingState, StreamReadingTrigger>( StreamReadingState.Initial ) )
        {
            StateMachine<StreamReadingState, StreamReadingTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Initial );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Initial );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Prime, StreamReadingState.Prime );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Ready, StreamReadingState.Ready );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Starting );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Busy );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Fail, StreamReadingState.Failed );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Complete );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Proceed, StreamReadingState.Prime );
            this.EngagedStates.Add( StreamReadingState.Prime );
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Prime );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Initial, StreamReadingState.Initial );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Prime );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Ready, StreamReadingState.Ready );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Starting, StreamReadingState.Starting );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Busy );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Fail, StreamReadingState.Failed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Complete, StreamReadingState.Completed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Proceed, StreamReadingState.Ready );
            this.EngagedStates.Add( StreamReadingState.Ready );
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Ready );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Initial, StreamReadingState.Initial );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Prime );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Ready );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Starting, StreamReadingState.Starting );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Busy );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Fail, StreamReadingState.Failed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Complete, StreamReadingState.Completed );
            this.EngagedStates.Add( StreamReadingState.Starting );
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Starting );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Initial, StreamReadingState.Initial );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Prime );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Ready );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Starting );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Busy, StreamReadingState.Busy );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Fail, StreamReadingState.Failed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Complete, StreamReadingState.Completed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Proceed, StreamReadingState.Busy );
            this.EngagedStates.Add( StreamReadingState.Busy );
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Busy );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Initial, StreamReadingState.Initial );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Prime );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Ready );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Starting );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Busy );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Fail, StreamReadingState.Failed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Complete, StreamReadingState.Completed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Proceed, StreamReadingState.Completed );
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Failed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Initial, StreamReadingState.Initial );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Prime );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Ready, StreamReadingState.Ready );
            _ = stateConfiguration.Ignore( StreamReadingTrigger.Starting );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Busy, StreamReadingState.Starting );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Fail );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Complete, StreamReadingState.Completed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Proceed, StreamReadingState.Initial );
            stateConfiguration = this.StateMachine.Configure( StreamReadingState.Completed );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Initial, StreamReadingState.Initial );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Prime, StreamReadingState.Prime );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Ready, StreamReadingState.Ready );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Starting, StreamReadingState.Starting );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Busy, StreamReadingState.Starting );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Fail, StreamReadingState.Failed );
            _ = stateConfiguration.PermitReentry( StreamReadingTrigger.Complete );
            _ = stateConfiguration.Permit( StreamReadingTrigger.Proceed, StreamReadingState.Initial );
        }

        #endregion

        #region " PROGRESS "

        /// <summary> Gets the state progress. </summary>
        /// <value> The state progress. </value>
        public int StateProgress => ( int ) (100 * ( int ) this.CurrentState / ( double ) StreamReadingState.Completed);

        /// <summary> The percent progress. </summary>
        private int _PercentProgress;

        /// <summary> The percent progress. </summary>
        /// <value> The percent progress. </value>
        public int PercentProgress
        {
            get => this._PercentProgress;

            set {
                value = Math.Min( 100, Math.Max( 0, value ) );
                if ( value != this.PercentProgress )
                {
                    this._PercentProgress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Updates the percent progress. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UpdatePercentProgress()
        {
            if ( this.CurrentState != StreamReadingState.Initial )
            {
                this.PercentProgress = this.StateProgress;
            }
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Process the stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void ProcessStopRequest()
        {
            if ( this.StoppingTimeoutElapsed() )
            {
                this.Fire( StreamReadingTrigger.Initial );
            }
            else
            {
                switch ( this.CurrentState )
                {
                    case StreamReadingState.Starting:
                        {
                            this.Fire( StreamReadingTrigger.Initial );
                            break;
                        }

                    case StreamReadingState.Failed:
                        {
                            this.Fire( StreamReadingTrigger.Initial );
                            break;
                        }

                    case StreamReadingState.Initial:
                        {
                            this.Fire( StreamReadingTrigger.Initial );
                            break;
                        }

                    case StreamReadingState.Prime:
                        {
                            this.Fire( StreamReadingTrigger.Complete );
                            break;
                        }

                    case StreamReadingState.Busy:
                        {
                            this.Fire( StreamReadingTrigger.Complete );
                            break;
                        }

                    case StreamReadingState.Completed:
                        {
                            this.Fire( StreamReadingTrigger.Initial );
                            break;
                        }

                    case StreamReadingState.Ready:
                        {
                            this.Fire( StreamReadingTrigger.Complete );
                            break;
                        }

                    default:
                        {
                            throw new InvalidOperationException( $"Unhanded {this.StateMachine.State} state processing stop" );
                        }
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent Stream Reading states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum StreamReadingState
    {

        /// <summary> An enum constant representing the initial state; Part not present. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the Prime state; Prepare streaming. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the Ready state; Ready to stream. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the Starting state; Stream is starting. </summary>
        [Description( "Starting" )]
        Starting,

        /// <summary> An enum constant representing the Busy state; Streaming is active; TO_DO: Rename to Streaming. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the failed state. Stream failed. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the Completed state. Stream completed. </summary>
        [Description( "Completed" )]
        Completed
    }

    /// <summary> Values that represent Stream Reading triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum StreamReadingTrigger
    {

        /// <summary> An enum constant representing the initial trigger. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the Prime trigger to Prepare streaming; go to the Prime state. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the Ready trigger to get Ready to stream; go to the ready state. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the Starting trigger to start streaming; go to starting state. </summary>
        [Description( "Starting" )]
        Starting,

        /// <summary> An enum constant representing the Busy trigger to activate streaming; go to the Busy state. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the Complete trigger to complete streaming; Move to Completed state. </summary>
        [Description( "Complete" )]
        Complete,

        /// <summary> An enum constant representing the fail option. Streaming failed. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the Proceed trigger to move the process to the next state. </summary>
        [Description( "Proceed" )]
        Proceed
    }
}
