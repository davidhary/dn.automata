using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a On/Off process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class OnOffEngine : EngineBase<OnOffState, OnOffTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> The initial state. </param>
        public OnOffEngine( string name, OnOffState initialState ) : base( name, new StateMachine<OnOffState, OnOffTrigger>( initialState ) )
        {
            StateMachine<OnOffState, OnOffTrigger>.StateConfiguration stateConfiguration;

            this.EngagedStates.Add( OnOffState.Off );
            stateConfiguration = this.StateMachine.Configure( OnOffState.Off );
            _ = stateConfiguration.PermitReentry( OnOffTrigger.Off );
            _ = stateConfiguration.Permit( OnOffTrigger.On, OnOffState.On );

            this.EngagedStates.Add( OnOffState.On );
            stateConfiguration = this.StateMachine.Configure( OnOffState.On );
            _ = stateConfiguration.Permit( OnOffTrigger.Off, OnOffState.Off );
            _ = stateConfiguration.PermitReentry( OnOffTrigger.On );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public OnOffEngine( string name ) : this( name, OnOffState.Off )
        {
        }

        #endregion

        #region " COMMANDS "

        /// <summary>   Move to or reenter the On state. </summary>
        /// <remarks>   David, 2020-10-01. </remarks>
        public void On()
        {
            this.Fire( OnOffTrigger.On );
        }

        /// <summary>   Move to or reenter the Off state. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Off()
        {
            this.Fire( OnOffTrigger.Off );
        }

        #endregion

    }

    /// <summary> Values that represent on off states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum OnOffState
    {

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off,

        /// <summary> An enum constant representing the on option. </summary>
        [Description( "On" )]
        On

    }

    /// <summary> Values that represent on off triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum OnOffTrigger
    {

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off,

        /// <summary> An enum constant representing the on option. </summary>
        [Description( "On" )]
        On
    }
}
