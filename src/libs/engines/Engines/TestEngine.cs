﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary>
    /// Implements a finite automata for a Test process capable of monitoring a set of measurements.
    /// </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class TestEngine : EngineBase<TestState, TestTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public TestEngine( string name ) : base( name, new StateMachine<TestState, TestTrigger>( TestState.Initial ) )
        {
            StateMachine<TestState, TestTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( TestState.Initial );
            _ = stateConfiguration.Ignore( TestTrigger.Cancel );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Ignore( TestTrigger.Initial );
            _ = stateConfiguration.Permit( TestTrigger.Prime, TestState.Priming );
            _ = stateConfiguration.Ignore( TestTrigger.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
            this.EngagedStates.Add( TestState.Priming );
            stateConfiguration = this.StateMachine.Configure( TestState.Priming );
            _ = stateConfiguration.Permit( TestTrigger.Cancel, TestState.Canceled );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Ignore( TestTrigger.Prime );
            _ = stateConfiguration.Permit( TestTrigger.Ready, TestState.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
            this.EngagedStates.Add( TestState.Ready );
            stateConfiguration = this.StateMachine.Configure( TestState.Ready );
            _ = stateConfiguration.Permit( TestTrigger.Cancel, TestState.Canceled );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Ignore( TestTrigger.Prime );
            _ = stateConfiguration.Ignore( TestTrigger.Ready );
            _ = stateConfiguration.Permit( TestTrigger.Start, TestState.Busy );
            this.EngagedStates.Add( TestState.Busy );
            stateConfiguration = this.StateMachine.Configure( TestState.Busy );
            _ = stateConfiguration.Permit( TestTrigger.Cancel, TestState.Canceled );
            _ = stateConfiguration.Permit( TestTrigger.Complete, TestState.Completed );
            _ = stateConfiguration.Permit( TestTrigger.Fail, TestState.Failed );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Ignore( TestTrigger.Prime );
            _ = stateConfiguration.Ignore( TestTrigger.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
            stateConfiguration = this.StateMachine.Configure( TestState.Failed );
            _ = stateConfiguration.Permit( TestTrigger.Cancel, TestState.Canceled );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Ignore( TestTrigger.Prime );
            _ = stateConfiguration.Ignore( TestTrigger.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
            this.EngagedStates.Add( TestState.Completed );
            stateConfiguration = this.StateMachine.Configure( TestState.Completed );
            _ = stateConfiguration.Ignore( TestTrigger.Cancel );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Permit( TestTrigger.Finalize, TestState.Finalized );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Permit( TestTrigger.Prime, TestState.Priming );
            _ = stateConfiguration.Ignore( TestTrigger.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
            stateConfiguration = this.StateMachine.Configure( TestState.Finalized );
            _ = stateConfiguration.Ignore( TestTrigger.Cancel );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Permit( TestTrigger.Prime, TestState.Priming );
            _ = stateConfiguration.Permit( TestTrigger.Ready, TestState.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
            stateConfiguration = this.StateMachine.Configure( TestState.Canceled );
            _ = stateConfiguration.Ignore( TestTrigger.Cancel );
            _ = stateConfiguration.Ignore( TestTrigger.Complete );
            _ = stateConfiguration.Ignore( TestTrigger.Fail );
            _ = stateConfiguration.Ignore( TestTrigger.Finalize );
            _ = stateConfiguration.Permit( TestTrigger.Initial, TestState.Initial );
            _ = stateConfiguration.Ignore( TestTrigger.Prime );
            _ = stateConfiguration.Ignore( TestTrigger.Ready );
            _ = stateConfiguration.Ignore( TestTrigger.Start );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Cancels this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Cancel()
        {
            this.Fire( TestTrigger.Cancel );
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( TestTrigger.Initial );
        }

        /// <summary> Completes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Complete()
        {
            if ( this.StopRequested )
            {
                this.Fire( TestTrigger.Cancel );
            }
            else
            {
                this.Fire( TestTrigger.Complete );
            }
        }

        /// <summary> Tests finalize. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void FinalizeTest()
        {
            if ( this.StopRequested )
            {
                this.Fire( TestTrigger.Cancel );
            }
            else
            {
                this.Fire( TestTrigger.Finalize );
            }
        }

        /// <summary> Primes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Prime()
        {
            if ( this.StopRequested )
            {
                this.Fire( TestTrigger.Cancel );
            }
            else
            {
                this.Fire( TestTrigger.Prime );
            }
        }

        /// <summary> Readies this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Ready()
        {
            if ( this.StopRequested )
            {
                this.Fire( TestTrigger.Cancel );
            }
            else
            {
                this.Fire( TestTrigger.Ready );
            }
        }

        /// <summary> Starts this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Start()
        {
            if ( this.StopRequested )
            {
                this.Fire( TestTrigger.Cancel );
            }
            else
            {
                this.Fire( TestTrigger.Start );
            }
        }

        /// <summary> Test failed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Fail()
        {
            this.Fire( TestTrigger.Fail );
        }

        #endregion

    }

    /// <summary> Values that represent test states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum TestState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the priming option. </summary>
        [Description( "Priming" )]
        Priming,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the busy option. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the failed option. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the completed option. </summary>
        [Description( "Completed" )]
        Completed,

        /// <summary> An enum constant representing the finalized option. </summary>
        [Description( "Finalized" )]
        Finalized,

        /// <summary> An enum constant representing the canceled option. </summary>
        [Description( "Testing Canceled" )]
        Canceled
    }

    /// <summary> Values that represent test triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum TestTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the prime option. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the start option. </summary>
        [Description( "Start" )]
        Start,

        /// <summary> An enum constant representing the complete option. </summary>
        [Description( "Complete" )]
        Complete,

        /// <summary> An enum constant representing the finalize option. </summary>
        [Description( "Finalize" )]
        Finalize,

        /// <summary> An enum constant representing the fail option. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the cancel option. </summary>
        [Description( "Cancel test" )]
        Cancel
    }
}