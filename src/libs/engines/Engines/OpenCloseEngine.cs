﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for an Open/Close process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class OpenCloseEngine : EngineBase<OpenCloseState, OpenCloseTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> State of the initial. </param>
        public OpenCloseEngine( string name, OpenCloseState initialState ) : base( name, new StateMachine<OpenCloseState, OpenCloseTrigger>( initialState ) )
        {
            StateMachine<OpenCloseState, OpenCloseTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( OpenCloseState.Initial );
            _ = stateConfiguration.Permit( OpenCloseTrigger.Close, OpenCloseState.Close );
            _ = stateConfiguration.Permit( OpenCloseTrigger.Open, OpenCloseState.Open );
            _ = stateConfiguration.Ignore( OpenCloseTrigger.Initial );
            this.EngagedStates.Add( OpenCloseState.Open );
            stateConfiguration = this.StateMachine.Configure( OpenCloseState.Open );
            _ = stateConfiguration.Permit( OpenCloseTrigger.Initial, OpenCloseState.Initial );
            _ = stateConfiguration.Permit( OpenCloseTrigger.Close, OpenCloseState.Close );
            _ = stateConfiguration.Ignore( OpenCloseTrigger.Open );
            this.EngagedStates.Add( OpenCloseState.Close );
            stateConfiguration = this.StateMachine.Configure( OpenCloseState.Close );
            _ = stateConfiguration.Permit( OpenCloseTrigger.Initial, OpenCloseState.Initial );
            _ = stateConfiguration.Ignore( OpenCloseTrigger.Close );
            _ = stateConfiguration.Permit( OpenCloseTrigger.Open, OpenCloseState.Open );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public OpenCloseEngine( string name ) : this( name, OpenCloseState.Initial )
        {
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Opens this object. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void Open()
        {
            this.Fire( OpenCloseTrigger.Open );
        }

        /// <summary> Closes this object. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void Close()
        {
            this.Fire( OpenCloseTrigger.Close );
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void Initial()
        {
            this.Fire( OpenCloseTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent open close states. </summary>
    /// <remarks> David, 2020-08-22. </remarks>
    public enum OpenCloseState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the open] option. </summary>
        [Description( "Open" )]
        Open,

        /// <summary> An enum constant representing the close option. </summary>
        [Description( "Close" )]
        Close
    }

    /// <summary> Values that represent open close triggers. </summary>
    /// <remarks> David, 2020-08-22. </remarks>
    public enum OpenCloseTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the open option. </summary>
        [Description( "Open" )]
        Open,

        /// <summary> An enum constant representing the close option. </summary>
        [Description( "Close" )]
        Close
    }
}