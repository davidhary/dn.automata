using System;
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary>
    /// Implements a finite automata for a Test Location process sequencing the loading, testing and
    /// unloading of a single item under test.
    /// </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class TestLocationEngine : EngineBase<TestLocationState, TestLocationTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public TestLocationEngine( string name ) : base( name, new TestLocationStateMachine() )
        {
            StateMachine<TestLocationState, TestLocationTrigger>.StateConfiguration stateConfiguration;
            this.StatusInfo = new TestLocationStatusInfo();
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Initial );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Cancel );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Initial );
            _ = stateConfiguration.Permit( TestLocationTrigger.Insert, TestLocationState.Present );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Permit( TestLocationTrigger.Extract, TestLocationState.Absent );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removed );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
            this.EngagedStates.Add( TestLocationState.Absent );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Absent );
            _ = stateConfiguration.Permit( TestLocationTrigger.Cancel, TestLocationState.Removed );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Permit( TestLocationTrigger.Insert, TestLocationState.Present );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removed, TestLocationState.Removed );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Permit( TestLocationTrigger.TestEnd, TestLocationState.Removed );
            this.EngagedStates.Add( TestLocationState.Present );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Present );
            _ = stateConfiguration.Permit( TestLocationTrigger.Cancel, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Insert );
            _ = stateConfiguration.Permit( TestLocationTrigger.Ready, TestLocationState.Ready );
            _ = stateConfiguration.Permit( TestLocationTrigger.Extract, TestLocationState.Absent );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removed );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removing, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Permit( TestLocationTrigger.TestEnd, TestLocationState.Removing );
            this.EngagedStates.Add( TestLocationState.Ready );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Ready );
            _ = stateConfiguration.Permit( TestLocationTrigger.Cancel, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Permit( TestLocationTrigger.Insert, TestLocationState.Present );
            _ = stateConfiguration.PermitReentry( TestLocationTrigger.Ready );
            _ = stateConfiguration.Permit( TestLocationTrigger.Extract, TestLocationState.Absent );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removed, TestLocationState.Removed );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removing, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Permit( TestLocationTrigger.TestStart, TestLocationState.Testing );
            _ = stateConfiguration.Permit( TestLocationTrigger.TestEnd, TestLocationState.Removing );
            this.EngagedStates.Add( TestLocationState.Testing );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Testing );
            _ = stateConfiguration.Permit( TestLocationTrigger.Cancel, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Insert );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removed, TestLocationState.Removed );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removing, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Permit( TestLocationTrigger.TestEnd, TestLocationState.Tested );
            this.EngagedStates.Add( TestLocationState.Tested );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Tested );
            _ = stateConfiguration.Permit( TestLocationTrigger.Cancel, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Insert );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removed, TestLocationState.Removed );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removing, TestLocationState.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
            this.EngagedStates.Add( TestLocationState.Removing );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Removing );
            _ = stateConfiguration.PermitReentry( TestLocationTrigger.Cancel );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Insert );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Permit( TestLocationTrigger.Removed, TestLocationState.Removed );
            _ = stateConfiguration.PermitReentry( TestLocationTrigger.Removing );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
            this.EngagedStates.Add( TestLocationState.Removed );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Removed );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Cancel );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Insert );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removed );
            _ = stateConfiguration.Permit( TestLocationTrigger.RemoveException, TestLocationState.RemoveException );
            _ = stateConfiguration.Permit( TestLocationTrigger.Save, TestLocationState.Saving );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Cancel );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Initial );
            _ = stateConfiguration.Permit( TestLocationTrigger.Insert, TestLocationState.Present );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removed );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removing );
            _ = stateConfiguration.Ignore( TestLocationTrigger.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
            this.EngagedStates.Add( TestLocationState.Saving );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Saving );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Cancel );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Absent );
            _ = stateConfiguration.Permit( TestLocationTrigger.Insert, TestLocationState.Present );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removed );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removing );
            _ = stateConfiguration.Ignore( TestLocationTrigger.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Permit( TestLocationTrigger.SavingDone, TestLocationState.Saved );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
            stateConfiguration = this.StateMachine.Configure( TestLocationState.Saved );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Cancel );
            _ = stateConfiguration.Permit( TestLocationTrigger.Initial, TestLocationState.Absent );
            _ = stateConfiguration.Permit( TestLocationTrigger.Insert, TestLocationState.Present );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Ready );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Extract );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removed );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Removing );
            _ = stateConfiguration.Ignore( TestLocationTrigger.RemoveException );
            _ = stateConfiguration.Ignore( TestLocationTrigger.Save );
            _ = stateConfiguration.Ignore( TestLocationTrigger.SavingDone );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestStart );
            _ = stateConfiguration.Ignore( TestLocationTrigger.TestEnd );
        }

        #endregion

        #region " MACHINE "

        /// <summary> Gets or sets information describing the status. </summary>
        /// <value> Information describing the status. </value>
        public TestLocationStatusInfo StatusInfo { get; private set; }

        /// <summary> Gets or sets the test location status. </summary>
        /// <value> The presence status. </value>
        public TestLocationStatuses Statuses
        {
            get => this.StatusInfo.Statuses;

            set {
                if ( value != this.Statuses || value == TestLocationStatuses.None )
                {
                    this.StatusInfo.Statuses = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Report state change. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        protected override void OnTransitioned( StateMachine<TestLocationState, TestLocationTrigger>.Transition transition )
        {
            base.OnTransitioned( transition );
            if ( transition is object )
            {
                this.OnTransitioned( transition.Destination );
            }
        }

        /// <summary> Executes the transitioned action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="state"> The state. </param>
        private void OnTransitioned( TestLocationState state )
        {
            switch ( state )
            {
                case TestLocationState.Initial:
                    {
                        this.Statuses = TestLocationStatuses.None;
                        break;
                    }

                case TestLocationState.Absent:
                    {
                        this.Statuses = TestLocationStatuses.Vacant;
                        break;
                    }

                case TestLocationState.Present:
                    {
                        // this clears the absent status
                        this.Statuses = TestLocationStatuses.Full;
                        break;
                    }

                case TestLocationState.Testing:
                    {
                        // if the test location is used for testing, it means that the test location was full when 
                        // testing started. The test location is then said to be engaged.
                        this.Statuses |= TestLocationStatuses.Engaged;
                        break;
                    }

                case TestLocationState.Tested:
                    {
                        this.Statuses |= TestLocationStatuses.Tested;
                        break;
                    }

                case TestLocationState.Saved:
                    {
                        this.Statuses |= TestLocationStatuses.Saved;
                        break;
                    }

                case TestLocationState.Removed:
                    {
                        // once the device is removed, it is marked as removed.  
                        this.Statuses |= TestLocationStatuses.Removed;
                        break;
                    }

                case TestLocationState.RemoveException:
                    {
                        this.Statuses |= TestLocationStatuses.RemovalException;
                        break;
                    }
            }
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Sends the initial trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( TestLocationTrigger.Initial );
        }

        /// <summary> Sends the insert trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Insert()
        {
            this.Fire( TestLocationTrigger.Insert );
        }

        /// <summary> Sends the ready trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void ReadyToTest()
        {
            this.Fire( TestLocationTrigger.Ready );
        }

        /// <summary> Tests start. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void TestStart()
        {
            this.Fire( TestLocationTrigger.TestStart );
        }

        /// <summary> Sends the test end trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void TestEnd()
        {
            this.Fire( TestLocationTrigger.TestEnd );
        }

        /// <summary> Sends the ready to remove trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Extract()
        {
            this.Fire( TestLocationTrigger.Extract );
        }

        /// <summary> Removings this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Removing()
        {
            this.Fire( TestLocationTrigger.Removing );
        }

        /// <summary> Sends the remove trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Removed()
        {
            this.Fire( TestLocationTrigger.Removed );
        }

        /// <summary> Sends the remove exception trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void RemoveException()
        {
            this.Fire( TestLocationTrigger.RemoveException );
        }

        /// <summary> Sends the save trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Save()
        {
            this.Fire( TestLocationTrigger.Save );
        }

        /// <summary> Saving done. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void SavingDone()
        {
            this.Fire( TestLocationTrigger.SavingDone );
        }

        /// <summary> Cancels testing. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Cancel()
        {
            this.Statuses |= TestLocationStatuses.Canceled;
            this.Fire( TestLocationTrigger.Cancel );
        }

        #endregion

    }

    /// <summary> A test location state machine. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class TestLocationStateMachine : StateMachine<TestLocationState, TestLocationTrigger>
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public TestLocationStateMachine() : base( TestLocationState.Initial )
        {
        }
    }

    /// <summary> Values that represent test location states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum TestLocationState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the absent option. </summary>
        [Description( "Absent" )]
        Absent,

        /// <summary> An enum constant representing the present option. </summary>
        [Description( "Present" )]
        Present,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the testing option. </summary>
        [Description( "Testing" )]
        Testing,

        /// <summary> An enum constant representing the tested option. </summary>
        [Description( "Tested" )]
        Tested,

        /// <summary> An enum constant representing the removing option. </summary>
        [Description( "Removing" )]
        Removing,

        /// <summary> An enum constant representing the removed option. </summary>
        [Description( "Removed" )]
        Removed,

        /// <summary> An enum constant representing the remove exception option. </summary>
        [Description( "Remove Exception" )]
        RemoveException,

        /// <summary> An enum constant representing the saving option. </summary>
        [Description( "Saving Data" )]
        Saving,

        /// <summary> An enum constant representing the saved option. </summary>
        [Description( "Data Saved" )]
        Saved
    }

    /// <summary> Values that represent test location triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum TestLocationTrigger
    {

        /// <summary> . </summary>
        [Description( "Initial: Any -> Initial" )]
        Initial,

        /// <summary> . </summary>
        [Description( "Extract: Present; Ready -> Absent" )]
        Extract,

        /// <summary> An enum constant representing the insert option. </summary>
        [Description( "Insert" )]
        Insert,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the test start option. </summary>
        [Description( "Test Start" )]
        TestStart,

        /// <summary> An enum constant representing the test end option. </summary>
        [Description( "Test End" )]
        TestEnd,

        /// <summary> . </summary>
        [Description( "Removing: Any -> Removing" )]
        Removing,

        /// <summary> . </summary>
        [Description( "Removed: Removing -> Removed" )]
        Removed,

        /// <summary> An enum constant representing the remove exception option. </summary>
        [Description( "Remove Exception" )]
        RemoveException,

        /// <summary> An enum constant representing the save option. </summary>
        [Description( "Save" )]
        Save,

        /// <summary> An enum constant representing the saving done option. </summary>
        [Description( "Saving Done" )]
        SavingDone,

        /// <summary> An enum constant representing the cancel option. </summary>
        [Description( "Cancel testing" )]
        Cancel
    }

    /// <summary> Values that represent the test location statuses. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    [Flags]
    public enum TestLocationStatuses
    {

        /// <summary>   A binary constant representing the initial flag. </summary>
        [Description( "Initial" )]
        None,

        /// <summary>   A binary constant representing the vacant flag. </summary>
        [Description( "Vacant" )]
        Vacant = 1,

        /// <summary>   A binary constant representing the full flag. </summary>
        [Description( "Full" )]
        Full = Vacant << 1,

        /// <summary>   A binary constant representing the engages flag. </summary>
        [Description( "Engaged" )]
        Engaged = Full << 1,

        /// <summary>   A binary constant representing the tested flag. </summary>
        [Description( "Tested" )]
        Tested = Engaged << 1,

        /// <summary>   A binary constant representing the removed flag. </summary>
        [Description( "Removed" )]
        Removed = Tested << 1,

        /// <summary>   A binary constant representing the removal exception flag. </summary>
        [Description( "Removal Exception" )]
        RemovalException = Removed << 1,

        /// <summary>   A binary constant representing the saved flag. </summary>
        [Description( "Saved" )]
        Saved = RemovalException << 1,

        /// <summary>   A binary constant representing the canceled flag. </summary>
        [Description( "Canceled" )]
        Canceled = Saved << 1
    }

    /// <summary> Information about the test location status. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-19 </para>
    /// </remarks>
    public class TestLocationStatusInfo
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public TestLocationStatusInfo() : base()
        {
        }

        /// <summary> Gets or sets the statuses. </summary>
        /// <value> The statuses. </value>
        public TestLocationStatuses Statuses { get; set; }

        /// <summary> Query if this object is initial. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if initial; otherwise <c>false</c> </returns>
        public bool IsInitial()
        {
            return TestLocationStatuses.None == this.Statuses;
        }

        /// <summary> True if the test location is vacant or was vacant when testing was ready. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if vacant; otherwise <c>false</c> </returns>
        public bool IsVacant()
        {
            return TestLocationStatuses.Vacant == (this.Statuses & TestLocationStatuses.Vacant);
        }

        /// <summary> True if the test location is Full or was Full when testing was ready. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if Full; otherwise <c>false</c> </returns>
        public bool IsFull()
        {
            return TestLocationStatuses.Full == (this.Statuses & TestLocationStatuses.Full);
        }

        /// <summary> True if test location actions canceled. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if canceled; otherwise <c>false</c> </returns>
        public bool IsCanceled()
        {
            return TestLocationStatuses.Canceled == (this.Statuses & TestLocationStatuses.Canceled);
        }

        /// <summary>
        /// True if the test location is engaged in testing, that is if it was full when testing started.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if test location is engaged; otherwise <c>false</c> </returns>
        public bool IsEngaged()
        {
            return TestLocationStatuses.Engaged == (this.Statuses & TestLocationStatuses.Engaged);
        }

        /// <summary> Query if a part was removed from the test location. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if part removed; otherwise <c>false</c> </returns>
        public bool IsRemoved()
        {
            return TestLocationStatuses.Removed == (this.Statuses & TestLocationStatuses.Removed);
        }

        /// <summary> Query if this object is removal exception. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if removal exception; otherwise <c>false</c> </returns>
        public bool IsRemovalException()
        {
            return TestLocationStatuses.RemovalException == (this.Statuses & TestLocationStatuses.RemovalException);
        }

        /// <summary> Query if this object is saved. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if saved; otherwise <c>false</c> </returns>
        public bool IsSaved()
        {
            return TestLocationStatuses.Saved == (this.Statuses & TestLocationStatuses.Saved);
        }

        /// <summary> Query if this object is part tested. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <returns> <c>true</c> if part tested; otherwise <c>false</c> </returns>
        public bool IsTested()
        {
            return TestLocationStatuses.Tested == (this.Statuses & TestLocationStatuses.Tested);
        }
    }
}
