using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a On/Off process with an initial state. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class InitialOnOffEngine : EngineBase<InitialOnOffState, InitialOnOffTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> The initial state. </param>
        public InitialOnOffEngine( string name, InitialOnOffState initialState ) : base( name, new StateMachine<InitialOnOffState, InitialOnOffTrigger>( initialState ) )
        {
            StateMachine<InitialOnOffState, InitialOnOffTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( InitialOnOffState.Initial );
            _ = stateConfiguration.PermitReentry( InitialOnOffTrigger.Initial );
            _ = stateConfiguration.Permit( InitialOnOffTrigger.Off, InitialOnOffState.Off );
            _ = stateConfiguration.Permit( InitialOnOffTrigger.On, InitialOnOffState.On );
            this.EngagedStates.Add( InitialOnOffState.On );
            stateConfiguration = this.StateMachine.Configure( InitialOnOffState.On );
            _ = stateConfiguration.Permit( InitialOnOffTrigger.Initial, InitialOnOffState.Initial );
            _ = stateConfiguration.Permit( InitialOnOffTrigger.Off, InitialOnOffState.Off );
            _ = stateConfiguration.PermitReentry( InitialOnOffTrigger.On );
            this.EngagedStates.Add( InitialOnOffState.Off );
            stateConfiguration = this.StateMachine.Configure( InitialOnOffState.Off );
            _ = stateConfiguration.Permit( InitialOnOffTrigger.Initial, InitialOnOffState.Initial );
            _ = stateConfiguration.PermitReentry( InitialOnOffTrigger.Off );
            _ = stateConfiguration.Permit( InitialOnOffTrigger.On, InitialOnOffState.On );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public InitialOnOffEngine( string name ) : this( name, InitialOnOffState.Initial )
        {
        }

        #endregion

        #region " COMMANDs "

        /// <summary>   Move to or reenter the On state. </summary>
        /// <remarks>   David, 2020-10-01. </remarks>
        public void On()
        {
            this.Fire( InitialOnOffTrigger.On );
        }

        /// <summary>   Move to or reenter the Off state. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Off()
        {
            this.Fire( InitialOnOffTrigger.Off );
        }

        /// <summary>   Move to or reenter the Initial state. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( InitialOnOffTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent the Initial, On or Off states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum InitialOnOffState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the on option. </summary>
        [Description( "On" )]
        On,

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off
    }

    /// <summary> Values that represent the Initial, On or Off triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum InitialOnOffTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the on option. </summary>
        [Description( "On" )]
        On,

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off
    }
}
