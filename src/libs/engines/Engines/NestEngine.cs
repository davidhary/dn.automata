﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for monitoring part presence or absence. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class NestEngine : EngineBase<NestState, NestTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public NestEngine( string name ) : base( name, new StateMachine<NestState, NestTrigger>( NestState.Initial ) )
        {
            StateMachine<NestState, NestTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( NestState.Initial );
            _ = stateConfiguration.PermitReentry( NestTrigger.Initial );
            _ = stateConfiguration.Permit( NestTrigger.Insert, NestState.Occupied );
            _ = stateConfiguration.Permit( NestTrigger.Remove, NestState.Empty );
            this.EngagedStates.Add( NestState.Empty );
            stateConfiguration = this.StateMachine.Configure( NestState.Empty );
            _ = stateConfiguration.Permit( NestTrigger.Initial, NestState.Initial );
            _ = stateConfiguration.Permit( NestTrigger.Insert, NestState.Occupied );
            _ = stateConfiguration.Ignore( NestTrigger.Remove );
            this.EngagedStates.Add( NestState.Occupied );
            stateConfiguration = this.StateMachine.Configure( NestState.Occupied );
            _ = stateConfiguration.Permit( NestTrigger.Initial, NestState.Initial );
            _ = stateConfiguration.Ignore( NestTrigger.Insert );
            _ = stateConfiguration.Permit( NestTrigger.Remove, NestState.Emptied );
            stateConfiguration = this.StateMachine.Configure( NestState.Emptied );
            _ = stateConfiguration.Permit( NestTrigger.Initial, NestState.Initial );
            _ = stateConfiguration.Ignore( NestTrigger.Insert );
            _ = stateConfiguration.Ignore( NestTrigger.Remove );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Inserts this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Insert()
        {
            this.Fire( NestTrigger.Insert );
        }

        /// <summary> Removes the part. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Remove()
        {
            this.Fire( NestTrigger.Remove );
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( NestTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent nest states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum NestState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the empty option. </summary>
        [Description( "Empty" )]
        Empty,

        /// <summary> An enum constant representing the occupied option. </summary>
        [Description( "Occupied" )]
        Occupied,

        /// <summary> An enum constant representing the emptied option. Nest was full and was emptied. </summary>
        [Description( "Emptied" )]
        Emptied
    }

    /// <summary> Values that represent nest triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum NestTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the insert option. </summary>
        [Description( "Insert" )]
        Insert,

        /// <summary> An enum constant representing the remove option. </summary>
        [Description( "Remove" )]
        Remove
    }
}