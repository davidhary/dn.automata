# About

isr.Automata.Finite.Engines is a .Net library supporting finite automata engines.

# How to Use

```
TBD
```

# Key Features

* Includes a set of state machines (engines).

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.
* _TBD_ to be defined.

# Feedback

isr.Automata.Finite.Engines is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Automata Repository].

[Automata Repository]: https://bitbucket.org/davidhary/dn.automata

