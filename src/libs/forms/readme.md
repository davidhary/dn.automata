# About

isr.Automata.Finite.Forms is a .Net library supporting finite automata Windows Forms.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Automata.Finite.Forms is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Automata Repository].

[Automata Repository]: https://bitbucket.org/davidhary/dn.automata

