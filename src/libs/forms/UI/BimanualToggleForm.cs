using System.Diagnostics;

namespace isr.Automata.Finite.Forms
{

    /// <summary> Form for viewing the bi-manual toggle switch. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-09-14 </para>
    /// </remarks>
    public partial class BimanualToggleForm : System.Windows.Forms.Form
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-10-01. </remarks>
        public BimanualToggleForm()
        {
            this.InitializeComponent();
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.components?.Dispose();
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #endregion

    }
}
