using System.Diagnostics;

namespace isr.Automata.Finite.Forms
{

    public partial class EngineMonitor<TState, TTrigger>
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._TriggerStatusStrip = new System.Windows.Forms.StatusStrip();
            this._LastTriggerLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._EngineToolStrip = new System.Windows.Forms.ToolStrip();
            this._EngineInfoButton = new System.Windows.Forms.ToolStripSplitButton();
            this._PreviousStateTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._NameStatusStrip = new System.Windows.Forms.StatusStrip();
            this._EngineNameLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.infoProvider = new InfoProvider( this.components);
            this._TriggerStatusStrip.SuspendLayout();
            this._EngineToolStrip.SuspendLayout();
            this._NameStatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // _TriggerStatusStrip
            // 
            this._TriggerStatusStrip.AutoSize = false;
            this._TriggerStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this._TriggerStatusStrip.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._TriggerStatusStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this._TriggerStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._LastTriggerLabel});
            this._TriggerStatusStrip.Location = new System.Drawing.Point(0, 16);
            this._TriggerStatusStrip.Name = "_TriggerStatusStrip";
            this._TriggerStatusStrip.ShowItemToolTips = true;
            this._TriggerStatusStrip.Size = new System.Drawing.Size(147, 16);
            this._TriggerStatusStrip.SizingGrip = false;
            this._TriggerStatusStrip.TabIndex = 1;
            this._TriggerStatusStrip.Text = "Trigger Status Strip";
            // 
            // _LastTriggerLabel
            // 
            this._LastTriggerLabel.Margin = new System.Windows.Forms.Padding(0);
            this._LastTriggerLabel.Name = "_LastTriggerLabel";
            this._LastTriggerLabel.Size = new System.Drawing.Size(132, 16);
            this._LastTriggerLabel.Spring = true;
            this._LastTriggerLabel.Text = "<trigger>";
            this._LastTriggerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._LastTriggerLabel.ToolTipText = "Last trigger";
            // 
            // _EngineToolStrip
            // 
            this._EngineToolStrip.AutoSize = false;
            this._EngineToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._EngineToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._EngineToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._EngineInfoButton});
            this._EngineToolStrip.Location = new System.Drawing.Point(0, -6);
            this._EngineToolStrip.Name = "_EngineToolStrip";
            this._EngineToolStrip.Padding = new System.Windows.Forms.Padding(0);
            this._EngineToolStrip.Size = new System.Drawing.Size(147, 20);
            this._EngineToolStrip.TabIndex = 2;
            this._EngineToolStrip.Text = "Engine Tool Strip";
            // 
            // _EngineInfoButton
            // 
            this._EngineInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._EngineInfoButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._PreviousStateTextBox});
            this._EngineInfoButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this._EngineInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._EngineInfoButton.Margin = new System.Windows.Forms.Padding(1);
            this._EngineInfoButton.Name = "_EngineInfoButton";
            this._EngineInfoButton.Size = new System.Drawing.Size(26, 18);
            this._EngineInfoButton.Text = "i";
            this._EngineInfoButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this._EngineInfoButton.ToolTipText = "Current state + Additional info";
            // 
            // _PreviousStateTextBox
            // 
            this._PreviousStateTextBox.Name = "_PreviousStateTextBox";
            this._PreviousStateTextBox.Size = new System.Drawing.Size(100, 23);
            // 
            // _NameStatusStrip
            // 
            this._NameStatusStrip.AutoSize = false;
            this._NameStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this._NameStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._EngineNameLabel});
            this._NameStatusStrip.Location = new System.Drawing.Point(0, 0);
            this._NameStatusStrip.Name = "_NameStatusStrip";
            this._NameStatusStrip.ShowItemToolTips = true;
            this._NameStatusStrip.Size = new System.Drawing.Size(147, 16);
            this._NameStatusStrip.SizingGrip = false;
            this._NameStatusStrip.TabIndex = 3;
            this._NameStatusStrip.Text = "Name Status Strip";
            // 
            // _EngineNameLabel
            // 
            this._EngineNameLabel.Margin = new System.Windows.Forms.Padding(0);
            this._EngineNameLabel.Name = "_EngineNameLabel";
            this._EngineNameLabel.Size = new System.Drawing.Size(132, 16);
            this._EngineNameLabel.Spring = true;
            this._EngineNameLabel.Text = "<name>";
            this._EngineNameLabel.ToolTipText = "Automaton name";
            // 
            // errorProvider
            // 
            this.infoProvider.ContainerControl = this;
            // 
            // EngineMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(147, 14);
            this.Controls.Add(this._EngineToolStrip);
            this.Controls.Add(this._TriggerStatusStrip);
            this.Controls.Add(this._NameStatusStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Name = "EngineMonitor";
            this._TriggerStatusStrip.ResumeLayout(false);
            this._TriggerStatusStrip.PerformLayout();
            this._EngineToolStrip.ResumeLayout(false);
            this._EngineToolStrip.PerformLayout();
            this._NameStatusStrip.ResumeLayout(false);
            this._NameStatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoProvider)).EndInit();
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.StatusStrip _TriggerStatusStrip;
        private System.Windows.Forms.ToolStrip _EngineToolStrip;
        private System.Windows.Forms.ToolStripSplitButton _EngineInfoButton;
        private System.Windows.Forms.ToolStripStatusLabel _LastTriggerLabel;
        private System.Windows.Forms.ToolStripTextBox _PreviousStateTextBox;
        private System.Windows.Forms.StatusStrip _NameStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _EngineNameLabel;
        private InfoProvider infoProvider;
    }
}
