using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Automata.Finite.Forms
{

    public partial class BimanualToggle
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._ErrorLable = new System.Windows.Forms.Label();
            this._InfoLabel = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._AcknowledgeButton = new System.Windows.Forms.Button();
            this._RightPushButton = new System.Windows.Forms.CheckBox();
            this._LeftPushButton = new System.Windows.Forms.CheckBox();
            this._CurrentStateLabel = new System.Windows.Forms.Label();
            this._EStopButton = new System.Windows.Forms.Button();
            this._Layout = new System.Windows.Forms.TableLayoutPanel();
            this.Panel1 = new System.Windows.Forms.Panel();
            this._SetStateLabelLabel = new System.Windows.Forms.Label();
            this._OffStateLabelLabel = new System.Windows.Forms.Label();
            this._ResponseTimeLabelLabel = new System.Windows.Forms.Label();
            this._OnStateLabelLabel = new System.Windows.Forms.Label();
            this._GoStateLabeLabel = new System.Windows.Forms.Label();
            this._ResponseTimeLabel = new System.Windows.Forms.Label();
            this._GoStateLabel = new System.Windows.Forms.Label();
            this._SetStateLabel = new System.Windows.Forms.Label();
            this._OffStateLabel = new System.Windows.Forms.Label();
            this._EStopStateLabelLabel = new System.Windows.Forms.Label();
            this._OnStateLabel = new System.Windows.Forms.Label();
            this._EStopStateLabel = new System.Windows.Forms.Label();
            this._Layout.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _ErrorLable
            // 
            this._ErrorLable.BackColor = System.Drawing.SystemColors.Control;
            this._ErrorLable.Cursor = System.Windows.Forms.Cursors.Default;
            this._ErrorLable.Dock = System.Windows.Forms.DockStyle.Left;
            this._ErrorLable.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._ErrorLable.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ErrorLable.Location = new System.Drawing.Point(18, 256);
            this._ErrorLable.Name = "_ErrorLable";
            this._ErrorLable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._ErrorLable.Size = new System.Drawing.Size(368, 72);
            this._ErrorLable.TabIndex = 35;
            this._ErrorLable.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _InfoLabel
            // 
            this._InfoLabel.BackColor = System.Drawing.SystemColors.Control;
            this._InfoLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._InfoLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this._InfoLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._InfoLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._InfoLabel.Location = new System.Drawing.Point(18, 328);
            this._InfoLabel.Name = "_InfoLabel";
            this._InfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._InfoLabel.Size = new System.Drawing.Size(368, 55);
            this._InfoLabel.TabIndex = 28;
            this._InfoLabel.Text = "An OSHA-compliant Bi-Manual Enabler control with E-Stop.  This control is enabled" +
    " upon receiving an acknowledge signal after both push buttons are pressed within" +
    " a preset time interval.";
            this._InfoLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _AcknowledgeButton
            // 
            this._AcknowledgeButton.BackColor = System.Drawing.SystemColors.Control;
            this._AcknowledgeButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._AcknowledgeButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._AcknowledgeButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this._AcknowledgeButton.Location = new System.Drawing.Point(8, 140);
            this._AcknowledgeButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._AcknowledgeButton.Name = "_AcknowledgeButton";
            this._AcknowledgeButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._AcknowledgeButton.Size = new System.Drawing.Size(102, 34);
            this._AcknowledgeButton.TabIndex = 50;
            this._AcknowledgeButton.Text = "&ACKNOWLEDGE";
            this._ToolTip.SetToolTip(this._AcknowledgeButton, "Must acknowledge (handshake) to move on.");
            this._AcknowledgeButton.UseVisualStyleBackColor = false;
            this._AcknowledgeButton.Click += new System.EventHandler(this.AcknowledgeButton_Click);
            // 
            // _RightPushButton
            // 
            this._RightPushButton.Appearance = System.Windows.Forms.Appearance.Button;
            this._RightPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this._RightPushButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._RightPushButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._RightPushButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this._RightPushButton.Location = new System.Drawing.Point(247, 182);
            this._RightPushButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._RightPushButton.Name = "_RightPushButton";
            this._RightPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._RightPushButton.Size = new System.Drawing.Size(108, 46);
            this._RightPushButton.TabIndex = 47;
            this._RightPushButton.Text = "&RIGHT";
            this._RightPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._ToolTip.SetToolTip(this._RightPushButton, "Check left and right buttons quickly to enable.");
            this._RightPushButton.UseVisualStyleBackColor = false;
            this._RightPushButton.Click += new System.EventHandler(this.PushButton_CheckStateChanged);
            // 
            // _LeftPushButton
            // 
            this._LeftPushButton.Appearance = System.Windows.Forms.Appearance.Button;
            this._LeftPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this._LeftPushButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._LeftPushButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._LeftPushButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this._LeftPushButton.Location = new System.Drawing.Point(125, 182);
            this._LeftPushButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LeftPushButton.Name = "_LeftPushButton";
            this._LeftPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LeftPushButton.Size = new System.Drawing.Size(108, 46);
            this._LeftPushButton.TabIndex = 46;
            this._LeftPushButton.Text = "&LEFT";
            this._LeftPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._ToolTip.SetToolTip(this._LeftPushButton, "Check left and right buttons quickly to enable.");
            this._LeftPushButton.UseVisualStyleBackColor = false;
            this._LeftPushButton.Click += new System.EventHandler(this.PushButton_CheckStateChanged);
            // 
            // _CurrentStateLabel
            // 
            this._CurrentStateLabel.BackColor = System.Drawing.Color.Transparent;
            this._CurrentStateLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._CurrentStateLabel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._CurrentStateLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CurrentStateLabel.Location = new System.Drawing.Point(5, 5);
            this._CurrentStateLabel.Name = "_CurrentStateLabel";
            this._CurrentStateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CurrentStateLabel.Size = new System.Drawing.Size(129, 24);
            this._CurrentStateLabel.TabIndex = 39;
            this._CurrentStateLabel.Text = "<state>";
            this._ToolTip.SetToolTip(this._CurrentStateLabel, "Current State");
            // 
            // _EStopButton
            // 
            this._EStopButton.BackColor = System.Drawing.Color.Red;
            this._EStopButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._EStopButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._EStopButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this._EStopButton.Location = new System.Drawing.Point(8, 180);
            this._EStopButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._EStopButton.Name = "_EStopButton";
            this._EStopButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._EStopButton.Size = new System.Drawing.Size(100, 52);
            this._EStopButton.TabIndex = 44;
            this._EStopButton.Text = "&E-STOP";
            this._ToolTip.SetToolTip(this._EStopButton, "Click to go to the EStop state");
            this._EStopButton.UseVisualStyleBackColor = false;
            this._EStopButton.Click += new System.EventHandler(this.EStopButton_Click);
            // 
            // _Layout
            // 
            this._Layout.ColumnCount = 3;
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._Layout.Controls.Add(this.Panel1, 1, 1);
            this._Layout.Controls.Add(this._ErrorLable, 1, 2);
            this._Layout.Controls.Add(this._InfoLabel, 1, 3);
            this._Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Layout.Location = new System.Drawing.Point(0, 0);
            this._Layout.Name = "_Layout";
            this._Layout.RowCount = 5;
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._Layout.Size = new System.Drawing.Size(404, 401);
            this._Layout.TabIndex = 36;
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this._SetStateLabelLabel);
            this.Panel1.Controls.Add(this._OffStateLabelLabel);
            this.Panel1.Controls.Add(this._ResponseTimeLabelLabel);
            this.Panel1.Controls.Add(this._OnStateLabelLabel);
            this.Panel1.Controls.Add(this._GoStateLabeLabel);
            this.Panel1.Controls.Add(this._ResponseTimeLabel);
            this.Panel1.Controls.Add(this._AcknowledgeButton);
            this.Panel1.Controls.Add(this._RightPushButton);
            this.Panel1.Controls.Add(this._LeftPushButton);
            this.Panel1.Controls.Add(this._GoStateLabel);
            this.Panel1.Controls.Add(this._SetStateLabel);
            this.Panel1.Controls.Add(this._OffStateLabel);
            this.Panel1.Controls.Add(this._EStopStateLabelLabel);
            this.Panel1.Controls.Add(this._CurrentStateLabel);
            this.Panel1.Controls.Add(this._EStopButton);
            this.Panel1.Controls.Add(this._OnStateLabel);
            this.Panel1.Controls.Add(this._EStopStateLabel);
            this.Panel1.Location = new System.Drawing.Point(18, 20);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(368, 233);
            this.Panel1.TabIndex = 37;
            // 
            // _SetStateLabelLabel
            // 
            this._SetStateLabelLabel.AutoSize = true;
            this._SetStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            this._SetStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._SetStateLabelLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._SetStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._SetStateLabelLabel.Location = new System.Drawing.Point(308, 101);
            this._SetStateLabelLabel.Name = "_SetStateLabelLabel";
            this._SetStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SetStateLabelLabel.Size = new System.Drawing.Size(26, 14);
            this._SetStateLabelLabel.TabIndex = 42;
            this._SetStateLabelLabel.Text = "SET";
            this._SetStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _OffStateLabelLabel
            // 
            this._OffStateLabelLabel.AutoSize = true;
            this._OffStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            this._OffStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._OffStateLabelLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._OffStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._OffStateLabelLabel.Location = new System.Drawing.Point(227, 60);
            this._OffStateLabelLabel.Name = "_OffStateLabelLabel";
            this._OffStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._OffStateLabelLabel.Size = new System.Drawing.Size(27, 14);
            this._OffStateLabelLabel.TabIndex = 41;
            this._OffStateLabelLabel.Text = "OFF";
            this._OffStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _ResponseTimeLabelLabel
            // 
            this._ResponseTimeLabelLabel.BackColor = System.Drawing.Color.Transparent;
            this._ResponseTimeLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._ResponseTimeLabelLabel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._ResponseTimeLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ResponseTimeLabelLabel.Location = new System.Drawing.Point(6, 48);
            this._ResponseTimeLabelLabel.Name = "_ResponseTimeLabelLabel";
            this._ResponseTimeLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._ResponseTimeLabelLabel.Size = new System.Drawing.Size(122, 15);
            this._ResponseTimeLabelLabel.TabIndex = 48;
            this._ResponseTimeLabelLabel.Text = "Bi-Manual Timeout";
            // 
            // _OnStateLabelLabel
            // 
            this._OnStateLabelLabel.AutoSize = true;
            this._OnStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            this._OnStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._OnStateLabelLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._OnStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._OnStateLabelLabel.Location = new System.Drawing.Point(148, 101);
            this._OnStateLabelLabel.Name = "_OnStateLabelLabel";
            this._OnStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._OnStateLabelLabel.Size = new System.Drawing.Size(22, 14);
            this._OnStateLabelLabel.TabIndex = 45;
            this._OnStateLabelLabel.Text = "ON";
            this._OnStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _GoStateLabeLabel
            // 
            this._GoStateLabeLabel.AutoSize = true;
            this._GoStateLabeLabel.BackColor = System.Drawing.Color.Transparent;
            this._GoStateLabeLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._GoStateLabeLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._GoStateLabeLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._GoStateLabeLabel.Location = new System.Drawing.Point(229, 143);
            this._GoStateLabeLabel.Name = "_GoStateLabeLabel";
            this._GoStateLabeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._GoStateLabeLabel.Size = new System.Drawing.Size(23, 14);
            this._GoStateLabeLabel.TabIndex = 43;
            this._GoStateLabeLabel.Text = "GO";
            this._GoStateLabeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _ResponseTimeLabel
            // 
            this._ResponseTimeLabel.BackColor = System.Drawing.SystemColors.Control;
            this._ResponseTimeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._ResponseTimeLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._ResponseTimeLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._ResponseTimeLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._ResponseTimeLabel.Location = new System.Drawing.Point(7, 64);
            this._ResponseTimeLabel.Name = "_ResponseTimeLabel";
            this._ResponseTimeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._ResponseTimeLabel.Size = new System.Drawing.Size(51, 16);
            this._ResponseTimeLabel.TabIndex = 49;
            this._ResponseTimeLabel.Text = "0.2500";
            // 
            // _GoStateLabel
            // 
            this._GoStateLabel.BackColor = System.Drawing.Color.Transparent;
            this._GoStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._GoStateLabel.Location = new System.Drawing.Point(201, 130);
            this._GoStateLabel.Name = "_GoStateLabel";
            this._GoStateLabel.Size = new System.Drawing.Size(75, 37);
            this._GoStateLabel.TabIndex = 35;
            // 
            // _SetStateLabel
            // 
            this._SetStateLabel.BackColor = System.Drawing.Color.Transparent;
            this._SetStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._SetStateLabel.Location = new System.Drawing.Point(281, 90);
            this._SetStateLabel.Name = "_SetStateLabel";
            this._SetStateLabel.Size = new System.Drawing.Size(80, 36);
            this._SetStateLabel.TabIndex = 36;
            // 
            // _OffStateLabel
            // 
            this._OffStateLabel.BackColor = System.Drawing.Color.Transparent;
            this._OffStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._OffStateLabel.Location = new System.Drawing.Point(201, 48);
            this._OffStateLabel.Name = "_OffStateLabel";
            this._OffStateLabel.Size = new System.Drawing.Size(75, 37);
            this._OffStateLabel.TabIndex = 37;
            // 
            // _EStopStateLabelLabel
            // 
            this._EStopStateLabelLabel.AutoSize = true;
            this._EStopStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            this._EStopStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._EStopStateLabelLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._EStopStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._EStopStateLabelLabel.Location = new System.Drawing.Point(305, 19);
            this._EStopStateLabelLabel.Name = "_EStopStateLabelLabel";
            this._EStopStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._EStopStateLabelLabel.Size = new System.Drawing.Size(40, 14);
            this._EStopStateLabelLabel.TabIndex = 40;
            this._EStopStateLabelLabel.Text = "ESTOP";
            this._EStopStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _OnStateLabel
            // 
            this._OnStateLabel.BackColor = System.Drawing.Color.Transparent;
            this._OnStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._OnStateLabel.Location = new System.Drawing.Point(119, 89);
            this._OnStateLabel.Name = "_OnStateLabel";
            this._OnStateLabel.Size = new System.Drawing.Size(75, 37);
            this._OnStateLabel.TabIndex = 34;
            // 
            // _EStopStateLabel
            // 
            this._EStopStateLabel.BackColor = System.Drawing.Color.Transparent;
            this._EStopStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._EStopStateLabel.Location = new System.Drawing.Point(287, 6);
            this._EStopStateLabel.Name = "_EStopStateLabel";
            this._EStopStateLabel.Size = new System.Drawing.Size(75, 37);
            this._EStopStateLabel.TabIndex = 38;
            // 
            // BimanualToggle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._Layout);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BimanualToggle";
            this.Size = new System.Drawing.Size(404, 401);
            this._Layout.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.Label _ErrorLable;
        private System.Windows.Forms.Label _InfoLabel;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel Panel1;
        private System.Windows.Forms.Label _SetStateLabelLabel;
        private System.Windows.Forms.Label _OffStateLabelLabel;
        private System.Windows.Forms.Label _ResponseTimeLabelLabel;
        private System.Windows.Forms.Label _OnStateLabelLabel;
        private System.Windows.Forms.Label _GoStateLabeLabel;
        private System.Windows.Forms.Label _ResponseTimeLabel;
        private System.Windows.Forms.Button _AcknowledgeButton;
        private System.Windows.Forms.CheckBox _RightPushButton;
        private System.Windows.Forms.CheckBox _LeftPushButton;
        private System.Windows.Forms.Label _GoStateLabel;
        private System.Windows.Forms.Label _SetStateLabel;
        private System.Windows.Forms.Label _OffStateLabel;
        private System.Windows.Forms.Label _EStopStateLabelLabel;
        private System.Windows.Forms.Label _CurrentStateLabel;
        private System.Windows.Forms.Button _EStopButton;
        private System.Windows.Forms.Label _OnStateLabel;
        private System.Windows.Forms.Label _EStopStateLabel;
    }
}
