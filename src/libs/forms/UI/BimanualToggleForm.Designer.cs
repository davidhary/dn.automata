using System.Diagnostics;

namespace isr.Automata.Finite.Forms
{

    public partial class BimanualToggleForm
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._BimanualToggle = new isr.Automata.Finite.Forms.BimanualToggle();
            this.SuspendLayout();
            // 
            // _BimanualToggle
            // 
            this._BimanualToggle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._BimanualToggle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._BimanualToggle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._BimanualToggle.Location = new System.Drawing.Point(0, 0);
            this._BimanualToggle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._BimanualToggle.Name = "_BimanualToggle";
            this._BimanualToggle.Size = new System.Drawing.Size(422, 435);
            this._BimanualToggle.TabIndex = 0;
            // 
            // BimanualToggleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 435);
            this.Controls.Add(this._BimanualToggle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BimanualToggleForm";
            this.Text = "BimanualToggleForm";
            this.ResumeLayout(false);

        }

        private BimanualToggle _BimanualToggle;
    }
}
